import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {AuthContextProvider} from './context/Auth/AuthContext';

/* TOAST */
import 'react-toastify/dist/ReactToastify.css';

/* Apollo */
import client from './helpers/apollo';
import { ApolloProvider } from "@apollo/react-hooks";

ReactDOM.render(
  <React.StrictMode>
    <AuthContextProvider>
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
    </AuthContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
if (module.hot) module.hot.accept();