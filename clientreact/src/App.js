import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

/* Pages */
import HomePage from "./pages/Home/Home.page";
import LoginPage from "./pages/Login/Login.page";
import OrdenCompraPage from "./pages/OrdenCompra/OrdenCompra.page";
import EnviarcotizacionPage from "./pages/EnviarCotizacion";

/* Context */
import { useContext } from "react";
import { AuthContext } from "./context/Auth/AuthContext";
import RegisterPage from "./pages/Register/Register.page";
import Reclamo from "./pages/Reclamo/Reclamo";
import SolicCotizPage from "./pages/Solicitarcotizacion/SolicCotiz.page";
import NuevoPedidoPage from "./pages/NuevoPedido/NuevoPedido.page";

function App() {
  const { user } = useContext(AuthContext);
  return (
    <Router>
      <Switch>
        {/* Rutas Auth */}

        <Route path="/login">
          {user ? <Redirect to="/dashboard" /> : <LoginPage />}
        </Route>

        <Route path="/register">
          {user ? <Redirect to="/dashboard" /> : <RegisterPage />}
        </Route>

        
        {/* Ruta principal, falta cambiar segun sea el tipo de usuario //TODO */}
        <Route path="/dashboard">
          {user ? <HomePage /> : <Redirect to="/login" />}
        </Route>

       

        {/* Rutas Area */}

        <Route path="/nuevopedido">
          {user && user.tipo_usuario === "area" ? <NuevoPedidoPage /> : <Redirect to="/login" />}
        </Route>
        <Route path="/reclamo">
          {user && user.tipo_usuario === "area" ? <Reclamo /> : <Redirect to="/login" />}
        </Route>

        <Route path="/solicitarcotizacion">
          {user && user.tipo_usuario === "area" ? (
            <SolicCotizPage />
          ) : (
            <Redirect to="/login" />
          )}
        </Route>

        {/* Rutas Proveedor */}
        <Route path="/enviarCotizacion">
          {user && user.tipo_usuario === "proveedor" ? <EnviarcotizacionPage /> : <Redirect to="/login" />}
        </Route>

        {/* Rutas Operario Compra */}
        <Route path="/ordencompra">
          {user && user.tipo_usuario === "operario_compra"  ?<OrdenCompraPage/> :<Redirect to="/ordencompra" />}
        </Route>
          
        

        {/* Ruta 404 not found */}

        <Route path="*">
          <Redirect to="/dashboard" />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
