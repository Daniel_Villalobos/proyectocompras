import React from "react";
import {
    Bookmarks,
    CalendarToday,
    CalendarViewDay,
    Chat, GolfCourse,
    Group,
    QuestionAnswer,
    RssFeed,
    Videocam
} from "@material-ui/icons";
import "./Sidebar.css";

export default function Siderbar(){
    return (
       <div className="sidebar">
           <div className="sidebarWrapper">
               <ul className="sidebarList">
                   <li className="sidebarListItem">
                        <RssFeed />
                       <span className="sidebarListItemText">
                           Orden de compra
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <Chat />
                       <span className="sidebarListItemText">
                           Solicitud de cotización
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <Videocam />
                       <span className="sidebarListItemText">
                           Envío de cotización
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <Group />
                       <span className="sidebarListItemText">
                           Pedido
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <Bookmarks />
                       <span className="sidebarListItemText">
                           Productos
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <QuestionAnswer />
                       <span className="sidebarListItemText">
                           Proveedores
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <CalendarToday />
                       <span className="sidebarListItemText">
                           Operarios
                       </span>
                   </li>
                   <li className="sidebarListItem">
                       <CalendarViewDay />
                       <span className="sidebarListItemText">
                           Balance y Kpi's
                       </span>
                   </li>
                   <li className="sidebarListItem">
                   <GolfCourse />
                   <span className="sidebarListItemText">
                           Courses
                       </span>
                    </li>
               </ul>
               <button className="sidebarButton">
                   Show More
               </button>
               <hr className="sidebarHr"/>
           </div>
       </div>
    )
}