import { useQuery } from '@apollo/react-hooks';
import { CircularProgress } from '@material-ui/core';
import gql from 'graphql-tag';
import { useState } from 'react';
import './Form.css';
import { useEffect } from 'react';
const GET_TIPOS_DOC_IDENTIFICACION = gql`
{
    tipos_documentos_identificacion{
        id_tipo_doc_ident
        nombre
        cantidad_digitos
    }
}
`
export default function Form0(props) {
    const { loading, data } = useQuery(GET_TIPOS_DOC_IDENTIFICACION);
    const { state, dispatch } = props.store;

    const [tipo_doc_ident, setTipo_doc_ident] = useState(state.form0.tipo_doc_ident)
    const [valor, setValor] = useState(state.form0.valor);

    const handle_tipo_doc_ident = (e) => {
        const id = e.target.value;
        const tipo = data.tipos_documentos_identificacion.filter(tdi => tdi.id_tipo_doc_ident === id)[0]
        setTipo_doc_ident(tipo);
    }
    const handleInput = (e) => {
        const valor = e.target.value;
        setValor(valor);
    }

    useEffect(() => {
        if(valor.length > 0){
            if(valor.length === tipo_doc_ident.cantidad_digitos){
                dispatch({
                    type: "FORM0",
                    payload: {
                        error: null,
                        tipo_doc_ident,
                        valor
                    }
                });
            }else{
                dispatch({
                    type: "FORM0",
                    payload: {
                        error: `El campo ${tipo_doc_ident.nombre} debe tener ${tipo_doc_ident.cantidad_digitos} digitos`,
                        tipo_doc_ident,
                        valor
                    }
                })
            }
        }else{
            dispatch({
                type: "FORM0",
                payload: {
                    error: `Completa el campo ${tipo_doc_ident.nombre}`,
                    tipo_doc_ident,
                    valor
                }
            })
        }
    }, [valor, tipo_doc_ident, dispatch]);

    if(loading) return <CircularProgress />
    return (
      <>
        <div className="form-group">
            <label className="labelTitle">Tipo de documento de identificación</label>
            <select className="formSelect" onChange={handle_tipo_doc_ident} value={tipo_doc_ident.id_tipo_doc_ident}>
                {
                    data.tipos_documentos_identificacion.map((tdi, index) => (
                        <option value={tdi.id_tipo_doc_ident} key={index}>{tdi.nombre}</option>
                    ))
                }
            </select>       
        </div>
        <div className="form-group">
            <label className="labelTitle">Escribe tu {tipo_doc_ident.nombre}</label>
            <input className="formInput" type="text" onChange={handleInput} value={valor}/>
        </div>
      </>
    );
}