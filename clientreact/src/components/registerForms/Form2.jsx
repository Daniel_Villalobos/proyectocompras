import { useQuery } from "@apollo/react-hooks";
import { CircularProgress } from "@material-ui/core";
import gql from "graphql-tag";
import { useState, useEffect } from "react";
import "./Form.css";
import TableContactos from "./TableContactos";
import { ToastContainer, toast } from "react-toastify";

const GET_TIPOS_CONTACTO = gql`
  {
    tipos_contacto {
      id_tipo_contacto
      nombre
      descripcion
    }
  }
`;
const GET_CONTACTOS = gql`
  {
    contactos {
      valor
    }
  }
`;
export default function Form0(props) {
  const { loading, data } = useQuery(GET_TIPOS_CONTACTO);
  const { loading: loadContacto, data: dataContacto } = useQuery(GET_CONTACTOS);
  const { state, dispatch } = props.store;

  const [contactos, setContactos] = useState(state.form2.contactos);

  const [rows, setRows] = useState(null);
  useEffect(() => {
    if (contactos.length > 0) {
      dispatch({
        type: "FORM2",
        payload: {
          error: null,
          contactos,
        },
      });
      setRows(
        contactos.map((c) => ({
          tipo_contacto: c.nombre,
          valor: c.valor,
        }))
      );
    }
  }, [contactos, dispatch]);
  const [tipo_contacto, setTipo_contacto] = useState({
    id_tipo_contacto: "a9a225ac-74f3-4973-a34a-74111446a6e9",
    nombre: "TEL",
    descripcion: "telefono celular",
  });
  const [valor, setValor] = useState("");

  const handle_tipo_contacto = (e) => {
    const id = e.target.value;
    const tipo = data.tipos_contacto.filter(
      (tc) => tc.id_tipo_contacto === id
    )[0];
    setTipo_contacto(tipo);
  };
  const handleInput = (e) => {
    const valor = e.target.value;
    setValor(valor);
  };
  const addContacto = () => {
    if (valor.length > 0) {
      if (dataContacto.contactos.some((c) => c.valor === valor)) {
        toast.error(`El ${tipo_contacto.nombre} ${valor} ya existe`)
      } else {
        toast.success(`Has agregado el ${tipo_contacto.nombre} ${valor}`);
        const newContacto = {
          id_tipo_contacto: tipo_contacto.id_tipo_contacto,
          nombre: tipo_contacto.nombre,
          valor: valor,
        };
        setContactos([...contactos, newContacto]);
        setValor("");
      }
    } else {
      toast.error(`Completa el campo ${tipo_contacto.descripcion}`);
    }
  };
  if (loading || loadContacto) return <CircularProgress />;
  return (
    <>
      <div className="form2">
        <div className="formcontactos">
          <div className="form-group">
            <label className="labelTitle">Escribe tu tipo de contacto</label>
            <select
              className="formSelect formselectcontacto"
              onChange={handle_tipo_contacto}
              value={tipo_contacto.id_tipo_contacto}
            >
              {data.tipos_contacto.map((tc, index) => (
                <option value={tc.id_tipo_contacto} key={index}>
                  {tc.nombre}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <label className="labelTitle">{tipo_contacto.descripcion}</label>
            <input
              className="formInput"
              type="text"
              onChange={handleInput}
              value={valor}
            />
          </div>
          <div className="form-group form-button">
            <button
              type="button"
              onClick={addContacto}
              className="formButton formbtncontacto"
            >
              Agregar
            </button>
          </div>
        </div>
        {rows && <TableContactos rows={rows} />}
      </div>
      <ToastContainer />
    </>
  );
}
