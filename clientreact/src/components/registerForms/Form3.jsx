import gql from "graphql-tag";
import React from "react";
import { useState, useEffect } from "react";
import { CircularProgress } from "@material-ui/core";
import { useQuery } from "@apollo/react-hooks";
import "./Form.css";

function FormArea({ store }) {
  const GET_EMPRESAS = gql`
    {
      empresas {
        id_empresa
        nombre
      }
    }
  `;
  const { state, dispatch } = store;
  const { loading, data } = useQuery(GET_EMPRESAS);
  const [nombreArea, setNombreArea] = useState(state.form3.area.nombre);
  const [selectEmpresa, setSelectEmpresa] = useState(
    state.form3.area.id_empresa
  );
  useEffect(() => {
    if (nombreArea.length > 0) {
      dispatch({
        type: "FORM3",
        payload: {
          error: null,
          tipo_usuario: "area",
          area: {
            id_empresa: selectEmpresa,
            nombre: nombreArea,
          },
          proveedor: {
            id_nacionalidad: "62c1e416-ffdb-4c1f-b79d-7ef9444cbdd8",
          },
          operario_compra: {
            rol_operario: {
              id_rol_operario: "558e8ea9-fa50-4126-811e-4ff7327ea76b",
            },
          },
        },
      });
    } else {
      dispatch({
        type: "FORM3",
        payload: {
          error: "Completa el campo nombre",
          tipo_usuario: "area",
          area: {
            id_empresa: selectEmpresa,
            nombre: "",
          },
          proveedor: {
            id_nacionalidad: "62c1e416-ffdb-4c1f-b79d-7ef9444cbdd8",
          },
          operario_compra: {
            rol_operario: {
              id_rol_operario: "558e8ea9-fa50-4126-811e-4ff7327ea76b",
            },
          },
        },
      });
    }
  }, [selectEmpresa, nombreArea, dispatch]);

  if (loading) return <CircularProgress />;
  return (
    <>
      <div className="form-group">
        <label className="labelTitle labelArea">Empresa</label>
        <select
          className="formSelect"
          style={{ width: "100%" }}
          value={selectEmpresa}
          onChange={(e) => setSelectEmpresa(selectEmpresa)}
        >
          {data.empresas.map((empresa, index) => (
            <option value={empresa.id_empresa} key={index}>
              {empresa.nombre}
            </option>
          ))}
        </select>
      </div>
      <div className="form-group">
        <label className="labelTitle labelArea">Nombre</label>
        <input
          className="formInput"
          style={{ maxWidth: "200px" }}
          type="text"
          onChange={(e) => setNombreArea(e.target.value)}
          value={nombreArea}
        />
      </div>
    </>
  );
}

function FormProveedor({ store }) {
  const GET_NACIONALIDADES = gql`
    {
      nacionalidades {
        id_nacionalidad
        descripcion
      }
    }
  `;
  const { loading, data } = useQuery(GET_NACIONALIDADES);
  const { state, dispatch } = store;
  const [nacion, setNacion] = useState(state.form3.proveedor.id_nacionalidad);

  useEffect(() => {
    dispatch({
      type: "FORM3",
      payload: {
        error: null,
        tipo_usuario: "proveedor",
        area: {
          id_empresa: "c822dad8-9dfd-4a24-85b9-06ed5ad8cfbf",
          nombre: "",
        },
        proveedor: {
          id_nacionalidad: nacion,
        },
        operario_compra: {
          rol_operario: {
            id_rol_operario: "558e8ea9-fa50-4126-811e-4ff7327ea76b",
          },
        },
      },
    });
  }, [nacion, dispatch]);
  if (loading) return <CircularProgress />;

  return (
    <>
      <div className="form-group">
        <label className="labelTitle labelProveedor">Nacionalidad</label>
        <select
          className="formSelect"
          style={{ width: "100%" }}
          onChange={(e) => setNacion(e.target.value)}
          value={nacion}
        >
          {data.nacionalidades.map((nacionalidad, index) => (
            <option value={nacionalidad.id_nacionalidad} key={index}>
              {nacionalidad.descripcion}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

function FormOperarioCompras({ store }) {
  const GET_ROLES_COMPRAS = gql`
    {
      roles_operario {
        id_rol_operario
        nombre
        descripcion
      }
    }
  `;
  const { loading, data } = useQuery(GET_ROLES_COMPRAS);
  const { state, dispatch } = store;
  const [rol, setRol] = useState(
    state.form3.operario_compra.rol_operario.id_rol_operario
  );

  useEffect(() => {
    dispatch({
      type: "FORM3",
      payload: {
        error: null,
        tipo_usuario: "operario_compra",
        area: {
          id_empresa: "c822dad8-9dfd-4a24-85b9-06ed5ad8cfbf",
          nombre: "",
        },
        proveedor: {
          id_nacionalidad: "62c1e416-ffdb-4c1f-b79d-7ef9444cbdd8",
        },
        operario_compra: {
          rol_operario: {
            id_rol_operario: rol,
          },
        },
      },
    });
  }, [rol, dispatch]);
  if (loading) return <CircularProgress />;

  return (
    <>
      <div className="form-group">
        <label className="labelTitle labelOperario">Rol operario</label>
        <select
          className="formSelect"
          style={{ width: "100%" }}
          onChange={(e) => setRol(e.target.value)}
          value={rol}
        >
          {data.roles_operario.map((rol, index) => (
            <option value={rol.id_rol_operario} key={index}>
              {rol.descripcion}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

function Formselect({ children, controltipo }) {
  return (
    <div className="form3">
      <select
        className="formSelect form3select"
        value={controltipo.tipo_usuario}
        onChange={(e) => controltipo.setTipoUsuario(e.target.value)}
      >
        <option value="area">Area</option>
        <option value="proveedor">Proveedor</option>
        <option value="operario_compra">Operario Compras</option>
      </select>
      {children}
    </div>
  );
}
export default function Form3({ store }) {
  const [tipo_usuario, setTipoUsuario] = useState(
    store.state.form3.tipo_usuario
  );

  switch (tipo_usuario) {
    case "area":
      return (
        <Formselect controltipo={{ tipo_usuario, setTipoUsuario }}>
          <FormArea store={store} />
        </Formselect>
      );
    case "proveedor":
      return (
        <Formselect controltipo={{ tipo_usuario, setTipoUsuario }}>
          <FormProveedor store={store} />
        </Formselect>
      );
    case "operario_compra":
      return (
        <Formselect controltipo={{ tipo_usuario, setTipoUsuario }}>
          <FormOperarioCompras store={store} />
        </Formselect>
      );
    default:
      return <></>;
  }
}
