import { useEffect } from 'react';
import { useState } from 'react';
import './Form.css';
export default function Form1(props) {
    const { state, dispatch} = props.store;
    const [nombre, setNombre] = useState(state.form1.nombre);
    const [contrasenia, setContrasenia] = useState(state.form1.contrasenia)
    const [confirmContrasenia, setConfirmContrasenia] = useState(state.form1.confirmContrasenia)
    useEffect(() => {
      if(nombre.length > 0){
        if(nombre.length < 3){
          dispatch({
            type: "FORM1",
            payload: {
              error: "El campo nombre debe tener más de 2 caracteres",
              nombre,
              contrasenia,
              confirmContrasenia
            }
          })
        }else{
          if(contrasenia.length > 0){
            if(contrasenia.length < 6){
              dispatch({
                type: "FORM1",
                payload: {
                  error: "El campo contrasenia debe tener más de 5 caracteres",
                  nombre,
                  contrasenia,
                  confirmContrasenia
                }
              })
            }else{
              if(confirmContrasenia !== contrasenia){
                dispatch({
                  type: "FORM1",
                  payload: {
                    error: "Las contraseñas no coinciden",
                    nombre,
                    contrasenia,
                    confirmContrasenia
                  }
                })
              }else{
                dispatch({
                  type: "FORM1",
                  payload: {
                    error: null,
                    nombre,
                    contrasenia,
                    confirmContrasenia
                  }
                })
              }
            }
          }else{
            dispatch({
              type: "FORM1",
              payload: {
                error: "Completa el campo contrasenia",
                nombre,
                contrasenia,
                confirmContrasenia
              }
            })
          }
        }
      }else{
        dispatch({
          type: "FORM1",
          payload: {
            error: "Completa el campo nombre",
            nombre,
            contrasenia,
            confirmContrasenia
          }
        })
      }
    }, [nombre, contrasenia, confirmContrasenia, dispatch])
    return (
      <>
        <div className="form-group">
            <label className="labelTitle">Escribe tu nombre</label>
            <input className="formInput" type="text" onChange={(e) => setNombre(e.target.value)} value={nombre}/>
        </div>
        <div className="form-group">
            <label className="labelTitle">Escribe tu contrasenia</label>
            <input className="formInput" type="password" onChange={(e) => setContrasenia(e.target.value)} value={contrasenia}/>
        </div>
        <div className="form-group">
            <label className="labelTitle">Confirma tu contrasenia</label>
            <input className="formInput" type="password" onChange={(e) => setConfirmContrasenia(e.target.value)} value={confirmContrasenia}/>
        </div>
      </>
    );
}