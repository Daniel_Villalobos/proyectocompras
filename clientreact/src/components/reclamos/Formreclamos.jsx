import { useQuery } from "@apollo/react-hooks";
import { useContext } from "react";
import { AuthContext } from "../../context/Auth/AuthContext";
import { gql } from "graphql-tag";
import './Formreclamo.css';
const GET_RECLAMOS = gql`
query get_crea($id_area: String){
    reclamos(id_area: $id_area){
        id_reclamo
        descripcion
    }
}
`;

export default function Formreclamo(){
    
    const { user } = useContext(AuthContext);


    const { loading, data } = useQuery(GET_RECLAMOS, {
        variables: {
            id_area: user.id_usuario 
        }
    }); 
    

    if(loading){
        return <div>Loading ...</div>
    }
    return(
        <div>
            <p>Reclamo de {user.nombre}</p>
            {data.reclamos.map(reclamo => (
                <p>{reclamo.descripcion}</p>
            ))}
            

            
            
        </div>
        
    )
}