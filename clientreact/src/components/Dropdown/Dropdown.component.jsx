import React from 'react';
import { Link } from 'react-router-dom';
import './Dropdown.css';
function Dropdown() {
    const logout = () => {
        localStorage.removeItem("user");
        window.location.reload();
    }
    return (
        <div className="dropdown">
            <ul className="nav">
                <li>
                    <Link to="/" className="linkitem">Home</Link>
                    <ul>
                        <li>
                            <Link to="/dashboard" className="linkitem">Dashboard</Link>
                        </li>
                        <li>
                            <Link to="/login" className="linkitem" onClick={logout}>
                                Log out
                            </Link>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    );
}

export default Dropdown;