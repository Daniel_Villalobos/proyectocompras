import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ToastContainer, toast } from "react-toastify";

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles({
  root: {
    marginBottom: 50,
  },
  cardActionArea: {
    display: "flex",
  },
  media: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
});

export default function ProductoCard({ producto, dispatch, unidades_medidas }) {
  const classes = useStyles();
  const [cantidad, setCantidad] = useState(0);

  const [um, setUm] = useState("");
  const handleOpen = () => {
    setOpen(true);
  };

  const add = () => {
    if (cantidad <= 0) {
      setOpen(false);
      toast.error("No puedes tener cantidades menores o igual que cero");
    } else {
      if (um !== "") {
        setOpen(false);
        dispatch({
          type: "ADD",
          payload: {
            id_tipo_prod: producto.id_tipo_prod,
            precio_ref: producto.precio_ref,
            nombre_prod: producto.nombre_prod,
            um: unidades_medidas.filter(u => um=== u.id_unidad_medida)[0].nombre,
            id_um: um,
            cantidad,
          },
        });
      }else{
        setOpen(false);
        toast.error("Debes elegir una unidad de medida");
      }
    }
  };

  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const [openSelect, setOpenSelect] = React.useState(false);

  const handleChangeSelect = (event) => {
    setUm(event.target.value);
  };

  const handleCloseSelect = () => {
    setOpenSelect(false);
  };

  const handleOpenSelect = () => {
    setOpenSelect(true);
  };
  const title = `¿Cuánta es la cantidad de ${producto.nombre_prod} que deseas?`;
  return (
    <>
      <Card className={classes.root}>
        <CardActionArea className={classes.cardActionArea}>
          <CardMedia className={classes.media} title="Contemplative Reptile">
            <img src="/images/loginshop.jpg" style={{ width: "100%" }} />
          </CardMedia>

          <CardContent className={classes.content}>
            <Typography gutterBottom variant="h5" component="h2">
              {producto.nombre_prod}
            </Typography>
            <Typography
              variant="h5"
              component="h6"
              style={{ color: "forestgreen" }}
            >
              ${producto.precio_ref}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {producto.descripcion_prod}
            </Typography>
            <CardActions>
              <Button size="small" color="primary" onClick={handleOpen}>
                Agregar
              </Button>
              <Button size="small" color="primary">
                Leer más
              </Button>
            </CardActions>
          </CardContent>
        </CardActionArea>
      </Card>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Confirmar</DialogTitle>
        <DialogContent>
          <DialogContentText>{title}</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Cantidad"
            type="number"
            fullWidth
            value={cantidad}
            onChange={(e) => setCantidad(e.target.value)}
          />
          <div>
            <FormControl style={{minWidth: "150px"}}>
              <InputLabel id="demo-controlled-open-select-label">
                Unidad medida
              </InputLabel>
              <Select
                labelId="demo-controlled-open-select-label"
                id="demo-controlled-open-select"
                open={openSelect}
                onClose={handleCloseSelect}
                onOpen={handleOpenSelect}
                value={um}
                onChange={handleChangeSelect}
              >
                {unidades_medidas.map((u) => (
                  <MenuItem value={u.id_unidad_medida}>{u.nombre}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={add} add="primary">
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
      <ToastContainer />
    </>
  );
}
