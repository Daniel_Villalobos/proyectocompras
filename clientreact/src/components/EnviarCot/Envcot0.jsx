import React, { useRef, useContext } from "react";


import { CircularProgress } from "@material-ui/core";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/Auth/AuthContext";
import { ToastContainer, toast } from "react-toastify";

const GET_COTIZACION = gql`
  {
    cotizacion {
      id_cotizacion
    }
  }
`;
const GET_PROVEEDOR = gql`
  {
    proveedor {
      id_proveedor
      nombre
    }
  }
`;

