import React, { useRef, useContext } from "react";
import "./Login.page.css";
import { CircularProgress } from "@material-ui/core";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/Auth/AuthContext";
import { ToastContainer, toast } from "react-toastify";
import {
  loginStartAction,
  loginSuccessAction,
  loginFailureAction,
} from "../../context/Auth/AuthActions";

import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

const GET_USERS = gql`
  { 
    usuarios{
      nombre
      contrasenia
      id_usuario
      tipo_usuario
    }
  }
`;


export default function LoginPage() {
  const nombre = useRef();
  const contrasenia = useRef();

  const { isFetching, dispatch } = useContext(AuthContext);
  const {loading, data} = useQuery(GET_USERS);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(loginStartAction());

    const user = {
      nombre: nombre.current.value,
      contrasenia: contrasenia.current.value,
    };
    const authAccepted = data.usuarios.filter(u => u.nombre === user.nombre && u.contrasenia === user.contrasenia);
    
    if (authAccepted.length > 0) {
      const loginUser = authAccepted[0];
      const {contrasenia,...other} = loginUser;
      dispatch(loginSuccessAction(other));
    } else {
      dispatch(loginFailureAction({ error: "User not found!" }));
      toast.error("¡El usuario no existe!");
      cleanForm();
    }
  };
  const cleanForm = () => {
    nombre.current.value = "";
    contrasenia.current.value = "";
  }
  return loading ? <CircularProgress /> :
    <div className="login">
      <div className="loginWrapper">
        <div className="loginLeft">
          <h3 className="loginTitle">Sistema de compras</h3>
          <img
            className="loginImg"
            src="/images/loginshop.jpg"
            alt="Login shop"
          />
          <span className="loginDescription">
            Nuestro objetivo es mantener el funcionamiento adecuado de las
            compras en tu empresa
          </span>
        </div>
        <div className="loginRigth">
          <form className="loginForm" onSubmit={handleSubmit}>
            <input
              type="text"
              placeholder="Nombre de usuario"
              className="loginInput"
              ref={nombre}
              minLength={3}
              required
            />
            <input
              type="password"
              placeholder="Contraseña"
              className="loginInput"
              ref={contrasenia}
              minLength={6}
              required
            />
            <button className="loginButton" disabled={isFetching} type="submit">
              {isFetching ? (
                <CircularProgress style={{ color: "white" }} />
              ) : (
                "Entrar"
              )}
            </button>
            <span className="loginForgot">¿Olvidaste tu contraseña?</span>
            <Link to="/register" className="loginRegisterLink">
              Crear cuenta
            </Link>
          </form>
        </div>
      </div>
      <ToastContainer />
    </div>
  
}
