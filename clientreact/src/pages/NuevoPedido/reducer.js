export function reducer(state, action) {
  switch (action.type) {
    case "ADD":
      return {
        requerimientos: [...state.requerimientos, action.payload],
      };
    case "DEL":
      return {
        requerimientos: state.requerimientos.filter(
          (req) => req.nro !== action.payload
        ),
      };
    default:
      return state;
  }
}
