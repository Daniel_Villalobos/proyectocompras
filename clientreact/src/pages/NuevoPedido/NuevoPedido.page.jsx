import { useContext, useEffect, useState, useReducer } from "react";
import {
  Button,
  CircularProgress,
  TextField,
  Typography,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import "./NuevoPedido.css";
import { useMutation, useQuery } from "@apollo/react-hooks";
import ProductoCard from "../../components/ProductoCard/ProductoCard";
import { gql } from "graphql-tag";
import Siderbar from "../../components/Sidebar/Sidebar.component";
import Topbar from "../../components/Topbar/TopbarComponent";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { AuthContext } from "../../context/Auth/AuthContext";
import { reducer } from "./reducer";
const GET_PRODUCTS_20 = gql`
  query tipos_producto {
    tipos_producto {
      id_tipo_prod
      estado_producto {
        nombre
      }
      categoria_producto {
        familia {
          descripcion_familia
          rubro {
            id_rubro
            descripcion_rubro
          }
        }
        descripcion_linea
      }
      nombre_prod
      precio_ref
      descripcion_prod
      antiguedad
    }
  }
`;
const GET_UNIDADES_MEDIDA = gql`
  {
    unidades_medida {
      id_unidad_medida
      nombre
    }
  }
`;
const CREATE_PEDIDO = gql`
  mutation create_pedido($pedido: pedidoInput) {
    create_pedido(pedido: $pedido)
  }
`;
const CREATE_REQUERIMIENTO = gql`
  mutation create_requerimiento($requerimiento: requerimientoInput){
    create_requerimiento(requerimiento: $requerimiento)
  }
`;


//
export default function NuevoPedidoPage() {
  const { data, loading } = useQuery(GET_PRODUCTS_20);
  const { data: dataud, loading: loadingud } = useQuery(GET_UNIDADES_MEDIDA);
  const { user } = useContext(AuthContext);
  const [create_pedido] = useMutation(CREATE_PEDIDO);
  const [create_requerimiento] = useMutation(CREATE_REQUERIMIENTO);

  const [state, dispatch] = useReducer(reducer, {
    requerimientos: [],
  });
  const [rows, setRows] = useState(null);

  const crearpedido = () => {
    const id_area = user.id_usuario;
    create_pedido({
      variables: {
        id_area
      }
    }).then(({data}) => {
      const id_pedido = data.create_pedido
      return new Promise.all([
        state.requerimientos.map((req, index) => (
          create_requerimiento({
            variables: {
              nro_requerimiento: index + 1, 
              id_pedido: id_pedido, 
              cantidad: req.cantidad,
              id_tipo_prod: req.id_tipo_prod, 
              id_unidad_medida: req.id_um
            }
          })
        ))
      ]).then(() => console.log("Pedido creado con éxito!"))
    })
  };
  useEffect(() => {
    setRows(
      state.requerimientos.map((req) => ({
        cantidad: req.cantidad,
        precio: req.precio_ref,
        name: req.nombre_prod,
        um: req.um,
      }))
    );
  }, [state]);

  return loading || loadingud ? (
    <CircularProgress />
  ) : (
    <>
      <Topbar />
      <div className="pedido">
        <div className="pedidoWrapper">
          <Siderbar />
          <div className="pedidoCenter">
            <div className="pedidoCenterWrapper">
              <div className="pedidoBuscador">
                <SearchIcon className="searchIcon" />
                <TextField
                  id="standard-search"
                  fullWidth
                  label="Busca el producto"
                  type="search"
                />
              </div>
              <div className="productos">
                <div className="productosWrapper">
                  {data.tipos_producto.map((tp) => (
                    <ProductoCard
                      dispatch={dispatch}
                      producto={tp}
                      unidades_medidas={dataud.unidades_medida}
                      key={tp.id_tipo_prod}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
          <div className="pedidoRigth">
            <div className="pedidoRigthWrapper">
              <Typography variant="h5" gutterBottom component="h2">
                Agrega el producto que quieras ...
              </Typography>
              <div className="table" style={{ marginTop: "2rem" }}>
                <TableContainer component={Paper}>
                  <Table size="small" aria-label="a dense table">
                    <TableHead>
                      <TableRow>
                        <TableCell>Producto</TableCell>
                        <TableCell align="right">Cantidad</TableCell>
                        <TableCell align="right">Medida</TableCell>
                        <TableCell align="right">Precio ($)</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {rows?.length > 0 &&
                        rows.map((row) => (
                          <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                              {row.name}
                            </TableCell>
                            <TableCell align="right">{row.cantidad}</TableCell>
                            <TableCell align="right">{row.um}</TableCell>
                            <TableCell align="right">$ {row.precio}</TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
              {rows?.length > 0 && (
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={crearpedido}
                  >
                    Crear pedido
                  </Button>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
