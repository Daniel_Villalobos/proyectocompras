 import React, { useRef, useContext, useState } from "react";

 import { CircularProgress } from "@material-ui/core";
 import { Link } from "react-router-dom";
 import { AuthContext } from "../../context/Auth/AuthContext";
 import { ToastContainer, toast } from "react-toastify";
 import { gql } from "apollo-boost";
 import { useMutation, useQuery } from "@apollo/react-hooks";

 import Siderbar from "../../components/Sidebar/Sidebar.component";
import Topbar from "../../components/Topbar/TopbarComponent";

import './SolicCotiz.page.css'

const GET_PROVEEDOR = gql`
{
    proveedores{
      id_proveedor
      nacionalidad{
        descripcion
      }
      usuario{
        nombre
      }
      calif_prov
    }
}
`;
const GET_PEDIDO = gql`
query requerimientos($id_pedido: String){
    requerimiento(id_pedido: $id_pedido){
      nro_requerimiento
      id_pedido
      tipo_prod{
        id_tipo_prod
       nombre_prod
      }
    }
}
`;
const GET_PROVEEDORES = gql`
query proveedores{
  calificacion
}

`;
const add_cotizacion = gql`
  mutation addCotizacion($cotizacion: cotizacionInput) {
    create_cotizacion(cotizacion: $cotizacion)
  }
`;

/*
SELECT id_proveedor FROM proveedor_tipo_producto WHERE 
id_tipo_prod = '39f837cd-6fd4-4679-b910-ee0a66503af4';
*/

export default function SolicCotizPage() {

  const id_pedido = "u8ag12ok12312-12312";
  const { data, loading } = useQuery(GET_PEDIDO, {
    variables: {
      id_pedido: id_pedido
    }
  })
  const {usuario }= useContext(AuthContext)
  const {data: dataprov, loading: loadingprov} = useQuery(GET_PROVEEDORES);
  const {create_cotizacion} = useMutation(add_cotizacion);

  const [currentReq, setCurrentReq] = useState(null);
  const [fecha_min, setFechaMin] = useState(null);
  const [fecha_max, setFechaMax] = useState(null);
 
  
 //req obj = data.requerimiento.filter(req => req.nro_requerimiento === currentReq)[0].tipo_prod.id_tipo_prod;
  
  
  const add = () => {
   /* insert into cotizacion(fecha_entrega_min, fecha_entrega_max, id_pedido, nro_requerimiento, id_proveedor, id_operario)
values  ($1, $2, $3, $4, $5, $6); */
    // for id_proveedor in 
    // addCotizacion("!1i2h21j31232")
    // addCotizacion("!1i2h21j31232")
    //addCotizacion
    // addCotizacion
  }
  function addCotizacion(id_proveedor){
    //if(fechamin === null){
//
  //  }else{

    //}
   const  req = data.requerimiento.filter(req => req.nro_requerimiento === currentReq)[0]
    create_cotizacion({
      variables: {
        cotizacion: {
          fecha_entrega_min: fecha_min,
          fecha_entrega_max: fecha_max,
          id_pedido: req.id_pedido,
          nro_requerimiento: req.nro,
          id_proveedor:id_proveedor,
          id_operario: usuario.id_usuario
        }
      }
    })
    toast.success('creadotasmas')
  }

  return (loading || loadingprov)? <div>Esta cargando</div> : (
      <>
       <Topbar />
        <div className= "solicitarCotiz"> 
          <div className ="solicitarCotizWrapper">
            
            <select className="solicitarCotizCenter" value={currentReq} onChange={(e) =>  setCurrentReq(e.target.value)}>
              {
                data.requerimiento.map((req) => (
                  <option value={req.nro}>{req.tipo_prod.nombre_prod}</option>
                ))
              }
            </select>
            <input type="date" value={fecha_min} onChange={(e) => setFechaMin(e.target.value)}/> 
          </div>
          {currentReq ? <button onClick={add}>add</button> : <div>selecciconanasns</div>}
        </div>
      </>
    )
}
