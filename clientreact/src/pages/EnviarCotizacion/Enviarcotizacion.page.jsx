import { className } from 'postcss-selector-parser';
import React, { useRef, useContext } from "react";
import { CircularProgress } from "@material-ui/core";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/Auth/AuthContext";
import { ToastContainer, toast } from "react-toastify";
import { gql } from "apollo-boost";
import { useMutation } from "@apollo/react-hooks";
import { useHistory } from 'react-router-dom';
const GET_ENVIO = gql`
  query cotizacion{
    id_cotizacion
    descuento
    monto_total
    fecha_envio_prov
  }
` 
const ADD_ENVIO = gql`
  mutation addEnvio($cotizacion: cotizacionInput) {
    create_cotizacion(cotizacion: $cotizacion)
  }
`;
export default function EnviarcotizacionPage() {
  /*const id_cotizacion = "";
  const [currentReq, setCurrentReq] = useState(null);
  const [fecha_min, setFechaMin] = useState(null);
  const [fecha_max, setFechaMax] = useState(null);
  const [rows, setRows] = useState(null);
  const { data, loading } = useQuery(GET_ENVIO, {
    variables: {
      id_cotizacion: id_cotizacion
    }
  });
  function addDatosCot(id_cotizacion){
    req = data.requerimiento.filter(req=>req.nro_requerimiento===currentReq)[0]
    create_envio({
      variables:{
        cotizacion: {
          fecha_envio_prov: fenprov,
          descuento: desc,
          monto_total: montot,
      }
    }
  })
  toast.success('enviado')
  }
  
  const cancelar = () => {
      history.push("/login");
  }; 
  */
  const { data, loading } = useQuery(GET_ENVIO);
  const { user } = useContext(AuthContext);
  const [add_envio] = useMutation(ADD_ENVIO);
  const [state, dispatch] = useReducer(reducer, {
    requerimientos: [],
  });
  const [rows, setRows] = useState(null);

  const enviar = () => {
    const id_proveedor = user.id_usuario;
    create_envio({
      variables: {
        id_proveedor
      }
    }),then(({data}) => {
      const id_cotizacion= data.create_envio
      
    })
  };
  useEffect(() => {
    setRows(
      state.requerimientos.map((req) => ({
        fecha_envio_prov: fenprov,
        descuento: desc,
        monto_total: montot,
      }))
    );
  }, [state]);
  return(
    <div className="EnvioCotizacion">
      <div className="envioWrapper">
        <div className="envioRight">
          <h3 className="envioTitle">Envio de Cotizacion</h3>
          <h2 className="codigoCotizacion">{id_cotizacion}</h2>
          <span className="envioDescription">
            Seleccione la cotizacion
          </span>
        </div>
        <div className="envioLeft">
        <TableContainer component={Paper}>
          <Table size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>Cotizacion</TableCell>
                <TableCell align="right">fecha_envio_prov</TableCell>
                <TableCell align="right">descuento</TableCell>
                <TableCell align="right">monto_total</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows?.length > 0 &&
              rows.map((row) => (
              <TableRow key={row.id_cotizacion}>
              <TableCell component="th" scope="row">
                {row.name}
                </TableCell>
                  <TableCell align="right">{row.fecha_envio_prov}</TableCell>
                  <TableCell align="right">{row.descuento}</TableCell>
                  <TableCell align="right">$ {row.monto_total}</TableCell>
                </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
        <Button
          variant="contained"
          onClick={enviar}
          >
          Crear pedido
        </Button>
      </div>
      <ToastContainer />
    </div>
  )
}