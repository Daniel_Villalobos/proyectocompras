import { className } from "postcss-selector-parser";
import React from "react";
import ordenCompra from '.componets/ordenCompra';


import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";

const GET_ORDENCOMPRA = gql`
  query orden_de_compra{
    orden_de_compra{
      id_orden_compra
      id_proveedor
      id_operario
      requerimiento{
        id_pedido
        nro_requerimiento
      }
      fecha_envio_orden
    }
  }
`;
export default function OrdenCompra() {
  const [create_OrdenCompra] = useMutation(ADD_OrdenCompra);
  const history = useHistory();

  const handleCreated = () => {
    create_OrdenCompra({
      variables: {
        orden_compra: {
          id_orden_compra: "",
          id_proveedor: "",
          id_producto: "",
          nombre: "",
        },
      },
    });
  };
  const enviar = () => {};
  const cancelar = () => {
    history.push("/dashboard");
  };

  return (
    <div className="ordenCompra">
      <div>
        <h2 className="ordenCompraTitle">Orden de Compra</h2>
        
      </div>

      <button className="" onClick={cancelar}>
        Cancelar
      </button>
    </div>
  );
}
