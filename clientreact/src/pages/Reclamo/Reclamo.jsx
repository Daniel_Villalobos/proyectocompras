import React, {useState} from "react";
import "./Reclamo.css";
import Formreclamos from "../../components/reclamos/Formreclamos";

import gql from "graphql-tag";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";

const ADD_RECLAMO = gql`
  mutation addReclamo($reclamo: reclamoInput) {
    create_reclamo(reclamo: $reclamo)
  }
`;
const GET_TIPO_RECLAMO = gql`
  {
    tipos_reclamo {
      id_tipo_reclamo
      descripcion
    }
  }
`;
export default function Reclamo() {
  const id_orden_compra = "1425361728-14253617283-123";

  const [create_reclamo] = useMutation(ADD_RECLAMO);
  const { data, loading } = useQuery(GET_TIPO_RECLAMO);
  const history = useHistory();

  const [id_tipo_reclamo, setId_tipo_reclamo] = useState(null);
  const [descripcion_reclamo, setDescripcion_reclamo] = useState(null);

  const handleCreated = () => {
    if(id_tipo_reclamo !== null && descripcion_reclamo !== null){
      create_reclamo({
        variables: {
          reclamo: {
            id_orden_compra: id_orden_compra,
            id_tipo_reclamo: id_tipo_reclamo,
            descripcion_reclamo: descripcion_reclamo,
          },
        },
      });
    }
  };
  const cancelar = () => {
    history.push("/login");
  };
  return loading ? (
    <div>Cargando ...</div>
  ) : (
    <div className="reclamo">
      <div>
        <h2 className="reclamoTitle">Registro de reclamo</h2>

        <input
          type="text"
          value={descripcion_reclamo}
          onChange={(e) => setDescripcion_reclamo(e.target.value)}
        />
        <Formreclamos />

        <select value={id_tipo_reclamo} onChange={e => setId_tipo_reclamo(e.target.value)}>
          {data.tipos_reclamo.map(tp => (
            <option value={tp.id_tipo_reclamo}>{tp.descripcion}</option>
          ))}
        </select>
      </div>
      <button className="reclamoBtn reclamoBtnEnviar" onClick={handleCreated}>
        Enviar
      </button>
      <button className="reclamoBtn reclamoBtnCancelar" onClick={cancelar}>
        Cancelar
      </button>
    </div>
  );
}
