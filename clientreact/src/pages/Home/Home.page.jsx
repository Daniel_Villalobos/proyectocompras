import React from "react";
import { useContext } from "react";
import { AuthContext } from "../../context/Auth/AuthContext";
import "./Home.page.css";


export default function HomePage() {
  const {user} = useContext(AuthContext);
  
  const logout = () => {
    localStorage.removeItem("user");
    window.location.reload();
  }
  return (
    <div>
      <p>Hola {user.nombre}!</p>
      <button onClick={logout}>Log Out</button>
    </div>
  )
}
