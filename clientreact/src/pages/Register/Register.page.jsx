import React, { useReducer, useState } from "react";
import { useHistory } from "react-router-dom";
import "./Register.page.css";
import Form0 from "../../components/registerForms/Form0";
import Form1 from "../../components/registerForms/Form1";
import Form2 from "../../components/registerForms/Form2";
import Form3 from "../../components/registerForms/Form3";

import { ToastContainer, toast } from "react-toastify";
import { reducer } from "./reducer";

/* Ventana Modal */
import { CircularProgress } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";

const ADD_DOC_IDENT = gql`
  mutation addDocIdent($doc_ident: documento_identificacionInput) {
    create_documento_identificacion(documento_identificacion: $doc_ident)
  }
`;
const ADD_USUARIO = gql`
  mutation addUser($usuario: usuarioInput) {
    create_usuario(usuario: $usuario)
  }
`;
const ADD_CONTACTO = gql`
  mutation addContacto($contacto: contactoInput) {
    create_contacto(contacto: $contacto)
  }
`;
const ADD_AREA = gql`
  mutation createArea($area: areaInput) {
    create_area(area: $area)
  }
`;
const ADD_PROVEEDOR = gql`
  mutation createProveedor($proveedor: proveedorInput) {
    create_proveedor(proveedor: $proveedor)
  }
`;
const ADD_OPERARIO = gql`
  mutation createOperario($operario: operarioInput) {
    create_operario(operario: $operario)
  }
`;

export default function RegisterPage() {
  const history = useHistory();

  const [state, dispatch] = useReducer(reducer, {
    form0: {
      error: "Completa el campo DNI",
      tipo_doc_ident: {
        id_tipo_doc_ident: "f9ad4b09-c3f9-43b9-be60-9ab8731aad61",
        nombre: "DNI",
        cantidad_digitos: 8,
      },
      valor: "",
    },
    form1: {
      error: "Completa el campo nombre",
      nombre: "",
      contrasenia: "",
      confirmContrasenia: "",
    },
    form2: {
      error: "Debes tener por lo menos un contacto",
      contactos: [],
    },
    form3: {
      error: "Debes seleccionar un valor",
      tipo_usuario: "area",
      area: {
        id_empresa: "c822dad8-9dfd-4a24-85b9-06ed5ad8cfbf",
        nombre: "",
      },
      proveedor: {
        id_nacionalidad: "62c1e416-ffdb-4c1f-b79d-7ef9444cbdd8",
      },
      operario_compra: {
        rol_operario: {
          id_rol_operario: "558e8ea9-fa50-4126-811e-4ff7327ea76b",
        },
      },
    },
  });
  const forms = [
    <Form0 store={{ state, dispatch }} />,
    <Form1 store={{ state, dispatch }} />,
    <Form2 store={{ state, dispatch }} />,
    <Form3 store={{ state, dispatch }} />,
  ];
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleClose = () => {
    if (!loading) {
      setOpen(false);
    }
  };
  const [create_documento_identificacion] = useMutation(ADD_DOC_IDENT);
  const [create_usuario] = useMutation(ADD_USUARIO);
  const [create_contacto] = useMutation(ADD_CONTACTO);
  const [create_area] = useMutation(ADD_AREA);
  const [create_proveedor] = useMutation(ADD_PROVEEDOR);
  const [create_operario] = useMutation(ADD_OPERARIO);

  const handleCreated = () => {
    setLoading(true);
    const id_tipo_doc_ident = state.form0.tipo_doc_ident.id_tipo_doc_ident;
    const valor = state.form0.valor;
    create_documento_identificacion({
      variables: {
        doc_ident: {
          id_tipo_doc_ident,
          valor,
        },
      },
    })
      .then(({ data }) => {
        const id_doc_ident = data.create_documento_identificacion;
        const nombre = state.form1.nombre;
        const contrasenia = state.form1.contrasenia;
        const tipo_usuario = state.form3.tipo_usuario;
        return create_usuario({
          variables: {
            usuario: {
              nombre,
              contrasenia,
              id_doc_ident,
              tipo_usuario,
            },
          },
        });
      })
      .then(({ data }) => {
        const id_usuario = data.create_usuario;
        const contactos = state.form2.contactos.map((contacto) => ({
          id_usuario,
          id_tipo_contacto: contacto.id_tipo_contacto,
          valor: contacto.valor,
        }));

        const tipo_usuario = state.form3.tipo_usuario;
        if (tipo_usuario === "area") {
          return Promise.all([
            ...contactos.map((contacto) =>
              create_contacto({ variables: { contacto } })
            ),
            create_area({
              variables: {
                area: {
                  ...state.form3.area,
                  id_area: id_usuario,
                },
              },
            }),
          ]);
        } else if (tipo_usuario === "proveedor") {
          return Promise.all([
            ...contactos.map((contacto) =>
              create_contacto({ variables: { contacto } })
            ),
            create_proveedor({
              variables: {
                proveedor: {
                  ...state.form3.proveedor,
                  id_proveedor: id_usuario,
                },
              },
            }),
          ]);
        } else if (tipo_usuario === "operario_compra") {
          return Promise.all([
            ...contactos.map((contacto) =>
              create_contacto({ variables: { contacto } })
            ),
            create_operario({
              variables: { 
                operario: {
                  id_rol_operario: state.form3.operario_compra.rol_operario.id_rol_operario,
                  id_operario: id_usuario
                }},
            }),
          ]);
        }
      })
      .then(() => {
        setLoading(false);
        setOpen(false);
        history.push("/login");
      });
  };
  const [currentForm, setCurrentForm] = useState({
    index: 0,
    element: forms[0],
  });

  const volver = () => {
    setCurrentForm({
      index: currentForm.index - 1,
      element: forms[currentForm.index - 1],
    });
  };

  const cancelar = () => {
    history.push("/login");
  };

  const continuar = () => {
    switch (currentForm.index) {
      case 0:
        if (!state.form0.error) {
          setCurrentForm({
            index: currentForm.index + 1,
            element: forms[currentForm.index + 1],
          });
        } else {
          toast.error(state.form0.error);
        }
        break;
      case 1:
        if (!state.form1.error) {
          setCurrentForm({
            index: currentForm.index + 1,
            element: forms[currentForm.index + 1],
          });
        } else {
          toast.error(state.form1.error);
        }
        break;
      case 2:
        if (!state.form2.error) {
          setCurrentForm({
            index: currentForm.index + 1,
            element: forms[currentForm.index + 1],
          });
        } else {
          toast.error(state.form2.error);
        }
        break;
      case 3:
        if (!state.form3.error) {
          setOpen(true);
        } else {
          toast.error(state.form3.error);
        }
        break;
      default:
        break;
    }
  };

  return (
    <div className="register">
      <h2 className="registerTitle">Registro de usuario</h2>
      <div className="registerWrapper">
        <div className="registerContainer">
          <div className="registerTop">
            <div className="registerTopController">
              <ul className="registerList">
                <li
                  className={
                    "registerItem" +
                    (currentForm.index === 0 ? " registerItemActive" : "")
                  }
                >
                  Identificación
                </li>
                <li
                  className={
                    "registerItem" +
                    (currentForm.index === 1 ? " registerItemActive" : "")
                  }
                >
                  Usuario
                </li>
                <li
                  className={
                    "registerItem" +
                    (currentForm.index === 2 ? " registerItemActive" : "")
                  }
                >
                  Contactos
                </li>
                <li
                  className={
                    "registerItem" +
                    (currentForm.index === 3 ? " registerItemActive" : "")
                  }
                >
                  Tipo usuario
                </li>
              </ul>
            </div>
            <hr className="registerLine" />
          </div>
          <div className="registerCenter">
            <div className={"form"}>{currentForm.element}</div>
          </div>
          <div className="registerBottom">
            <hr className="registerLine" />
            <div className="registerBottomController">
              <div className="registerBottomControllerLeft">
                {currentForm.index > 0 && currentForm.index <= forms.length ? (
                  <button
                    className="registerBtn registerBtnVolver"
                    onClick={volver}
                  >
                    Volver
                  </button>
                ) : (
                  <></>
                )}
              </div>
              <div className="registerBottomControllerRight">
                <button
                  className="registerBtn registerBtnCancelar"
                  onClick={cancelar}
                >
                  Cancelar
                </button>
                <button
                  className="registerBtn registerBtnContinuar"
                  onClick={continuar}
                >
                  {currentForm.index === 3 ? "Crear" : "Continuar"}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Confirmación de usuario"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            ¿Estás seguro de crear este usuario?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" disabled={loading}>
            Cancelar
          </Button>
          <Button
            onClick={handleCreated}
            color="primary"
            autoFocus
            disabled={loading}
          >
            {loading ? <CircularProgress /> : "Aceptar"}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
