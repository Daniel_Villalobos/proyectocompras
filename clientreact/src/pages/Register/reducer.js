export function reducer(state, action) {
  switch (action.type) {
    case "FORM0":
      return {
        ...state,
        form0: action.payload,
      };
    case "FORM1":
      return {
        ...state,
        form1: action.payload,
      };
    case "FORM2":
      return {
        ...state,
        form2: action.payload,
      };
    case "FORM3":
      return {
        ...state,
        form3: action.payload,
      };
    default:
      return state;
  }
}
