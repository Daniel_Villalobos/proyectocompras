export const LOGIN_START = "LOGIN_START";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export function loginStartAction() {
  return {
    type: LOGIN_START,
  };
}
export function loginSuccessAction(userLogin) {
  return {
    type: LOGIN_SUCCESS,
    payload: userLogin,
  };
}
export function loginFailureAction(error) {
  return {
    type: LOGIN_FAILURE,
    payload: error,
  };
}
