const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql');

const partida_presupuestaria = new GraphQLObjectType({
    name: 'partida_presupuestaria',
    fields: () => ({
        monto_inicial: { type: GraphQLInt },
        monto_disponible: {type: GraphQLInt},
        monto_utilizado: {type: GraphQLInt},
        fecha_inicial: {type: GraphQLString},
        fecha_final: {type: GraphQLString},
        id_area: {type: GraphQLString},
        obj_gasto: {type: require("../objeto_gasto/type")}       
                
    })
})
module.exports = partida_presupuestaria;