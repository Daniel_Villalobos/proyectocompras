const contacto_controller = require('../contacto/c.controllers')
const get_partidas_presupuestaria_area = async(id_area,client) => {
    try{
        const res = await client.query(`
        SELECT
            PP.monto_inicial,
            PP.monto_disponible,
            PP.monto_utilizado,
            PP.fecha_inicial,
            PP.fecha_final,
            A.id_area,
            OG.id_obj_gasto,
            OG.descripcion as "descripcion"	
        FROM partida_presupuestaria PP
        INNER JOIN area A
        ON PP.id_area = A.id_area
        INNER JOIN objeto_gasto OG
        ON PP.id_obj_gasto = OG.id_obj_gasto
        WHERE PP.id_area = '${id_area}';

        `);
        partidas_presupuestaria = []
        for(const row of res.rows){
            partidas_presupuestaria.push({
                monto_inicial: row.monto_inicial,
                monto_disponible: row.monto_disponible,
                monto_utilizado: row.monto_utilizado,
                fecha_inicial:row.fecha_inicial,
                fecha_final: row.fecha_final,
                id_area: row.id_area,                       
                obj_gasto:{
                    id_obj_gasto: row.id_obj_gasto,
                    descripcion:row.descripcion
                     },
                             
            })
        }
        return partidas_presupuestaria;
    }
    catch(e){
        console.log(e)
    }
 }
const get_partidas_presupuestaria = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
        SELECT
            PP.monto_inicial,
            PP.monto_disponible,
            PP.monto_utilizado,
            PP.fecha_inicial,
            PP.fecha_final,
            A.id_area,
            OG.id_obj_gasto,
            OG.descripcion as "descripcion"	
        FROM partida_presupuestaria PP
        INNER JOIN area A
        ON PP.id_area = A.id_area
        INNER JOIN objeto_gasto OG
        ON PP.id_obj_gasto = OG.id_obj_gasto;

        `);
        partidas_presupuestaria = []
        for(const row of res.rows){
            partidas_presupuestaria.push({
                monto_inicial: row.monto_inicial,
                monto_disponible: row.monto_disponible,
                monto_utilizado: row.monto_utilizado,
                fecha_inicial: row.fecha_inicial,
                fecha_final: row.fecha_final,
                id_area: row.id_area,                       
                obj_gasto:{
                    id_obj_gasto: row.id_obj_gasto,
                    descripcion: row.descripcion
                     },
                             
            })
        }
        return partidas_presupuestaria;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
module.exports = {
    get_partidas_presupuestaria,
    get_partidas_presupuestaria_area
};

