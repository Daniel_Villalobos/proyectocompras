const { GraphQLList } = require('graphql')
const pp_controller = require('./pp.controllers')
const type_partida_presupuestaria = require('./type')

const query = {
    partidas_presupuestaria: {
        type: new GraphQLList(type_partida_presupuestaria),
        resolve: pp_controller.get_partidas_presupuestaria
    }
}
module.exports = query;