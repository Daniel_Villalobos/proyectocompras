const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const tipo_producto = new GraphQLObjectType({
    name: 'tipo_producto',
    fields: () => ({
        id_tipo_prod: { type: GraphQLString },
        estado_producto: { type: require('../estado_producto/type') },
        categoria_producto:{ type: require('../categoria_producto/type') },
        nombre_prod: { type: GraphQLString },
        precio_ref: { type: GraphQLInt },
        descripcion_prod: { type: GraphQLString },
        antiguedad: { type: GraphQLString }
    })
})
module.exports = tipo_producto;