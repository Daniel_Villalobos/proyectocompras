const get_tipos_producto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT 
                TP.id_tipo_prod,
                EP.id_estado,
                EP.nombre as "nombre_estado",
                CP.id_cat_prod,
                CP.descripcion_linea,
                F.id_familia,
                F.descripcion_familia,
                R.id_rubro,
                R.descripcion_rubro,
                TP.nombre_prod,
                TP.precio_ref,
                TP.descripcion_prod,
                TP.antiguedad
            FROM tipo_producto TP
            INNER JOIN estado_producto EP
            ON EP.id_estado = TP.id_estado
            INNER JOIN categoria_producto CP
            ON TP.id_cat_prod = CP.id_cat_prod
            INNER JOIN familia F
            ON CP.id_familia = F.id_familia
            INNER JOIN rubro R
            ON F.id_rubro = R.id_rubro;	
        `);
        const tipos_producto = []
        for(const row of res.rows){
            tipos_producto.push({
                id_tipo_prod: row.id_tipo_prod,
                estado_producto: {
                    id_estado: row.id_estado,
                    nombre: row.nombre_estado
                },
                categoria_producto: {
                    id_cat_prod: row.id_cat_prod,
                    descripcion_linea: row.descripcion_linea,
                    familia: {
                        id_familia: row.id_familia,
                        rubro: {
                            id_rubro: row.id_rubro,
                            descripcion_rubro: row.descripcion_rubro
                        },
                        descripcion_familia: row.descripcion_familia
                    }
                },
                nombre_prod: row.nombre_prod,
                precio_ref: row.precio_ref,
                descripcion_prod: row.descripcion_prod,
                antiguedad: row.antiguedad
            });
        }
        return tipos_producto;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

module.exports = {
    get_tipos_producto
};