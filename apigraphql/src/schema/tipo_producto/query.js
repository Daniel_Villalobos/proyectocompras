const { GraphQLList } = require('graphql')
const tp_controller = require('./tp.controller')
const type_tipo_producto = require('./type')

const query = {
    tipos_producto: {
        type: new GraphQLList(type_tipo_producto),
        resolve: tp_controller.get_tipos_producto
    }
}
module.exports = query;