const { GraphQLList } = require('graphql')
const ea_controller = require('./ea.controllers')
const type_estado_aprobacion = require('./type')

const query = {
    estado_aprobacion: {
        type: new GraphQLList(type_estado_aprobacion),
        resolve: ea_controller.get_estados_aprobacion
    }
}
module.exports = query;