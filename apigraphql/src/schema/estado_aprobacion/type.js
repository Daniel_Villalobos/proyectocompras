const {GraphQLObjectType, GraphQLInt, GraphQLString } = require('graphql')

const estados_aprobacion = new GraphQLObjectType({
    name: 'estado_aprobacion',
    fields: () => ({
        id_estado_aprob: { type: GraphQLInt },
        descripcion: { type: GraphQLString },
        
    })
})
module.exports = estados_aprobacion;