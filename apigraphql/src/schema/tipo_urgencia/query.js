const { GraphQLList } = require('graphql')
const tu_controllers = require('./tu.controllers')
const type_tipo_urgencia = require('./type')

const query = {
    tipo_urgencia: {
        type: new GraphQLList(type_tipo_urgencia),
        resolve: tu_controllers.get_tipo_urgencia
    }
}
module.exports = query;