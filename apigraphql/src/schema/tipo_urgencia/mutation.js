const tipo_urgencia = require("./type");
const tu_controller = require('./tu.controllers');
const tu_input = require('./tu.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_tipo_urgencia: {
        type: GraphQLString,
        args: {
            tipo_urgencia: { type: tu_input, required: true}
        },
        resolve: tu_controller.create_tipo_urgencia
    }
}
module.exports = mutation;