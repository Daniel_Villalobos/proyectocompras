const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const tipo_urgenciaInput = new GraphQLInputObjectType({
    name: 'tipo_urgenciaInput',
    fields: {
        descripcion_tipo_urg: { type: GraphQLString, require: true}
    }
})
module.exports = tipo_urgenciaInput;