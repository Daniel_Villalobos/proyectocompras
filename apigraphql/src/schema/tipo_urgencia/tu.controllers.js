const get_tipo_urgencia = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM tipo_urgencia;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_tipo_urgencia = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {descripcion_tipo_urg} = args.tipo_urgencia;
      
        const query = `
            INSERT INTO tipo_urgencia
            (descripcion_tipo_urg)
            VALUES($1) RETURNING id_tipo_urg`;
        
        const res = await client.query(query, [descripcion_tipo_urg]);
        const id = res.rows[0]["id_tipo_urg"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_tipo_urgencia,
    create_tipo_urgencia
};