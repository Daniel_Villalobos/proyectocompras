const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const tipo_urgencia = new GraphQLObjectType({
    name: 'tipo_urgencia',
    fields: () => ({
        id_tipo_urg: { type: GraphQLInt },
        descripcion_tipo_urg: { type: GraphQLString }
    })
})
module.exports = tipo_urgencia