const { GraphQLList } = require('graphql')
const pdp_controllers = require('./pdp.controllers')
const type_periodo_disponible_prov= require('./type')

const query = {
    periodos_disponible_prov: {
        type: new GraphQLList(type_periodo_disponible_prov),
        resolve: pdp_controllers.get_periodos_disponible_prov
    }
}
module.exports = query;