const {GraphQLObjectType, GraphQLString } = require('graphql')

const periodo_disponible_prov = new GraphQLObjectType({
    name: 'periodo_disponible_prov',
    fields: () => ({
        id_proveedor: { type: GraphQLString },
        fecha_inicial: { type: GraphQLString },
        fecha_final: { type: GraphQLString }
    })
})
module.exports = periodo_disponible_prov;