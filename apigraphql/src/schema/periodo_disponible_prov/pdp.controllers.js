const get_periodos_disponibles_user = async (id_user, client) => {
    try{
        const res = await client.query(`
            SELECT * FROM periodo_disponible_prov
            WHERE id_proveedor='${id_user}';
        `);
        return res.rows;
    }catch(e){
        console.log(e);
    }
}
const get_periodos_disponible_prov = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM periodo_disponible_prov;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
module.exports = {
    get_periodos_disponible_prov,
    get_periodos_disponibles_user
};
