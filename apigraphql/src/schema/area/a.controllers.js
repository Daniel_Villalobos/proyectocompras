const contacto_controller = require('../contacto/c.controllers')
const pp_controller = require('../partida_presupuestaria/pp.controllers')
const get_areas = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT
                A.id_area,
                A.nombre as "nombre_area",
                E.id_empresa,
                E.nombre as "nombre_empresa",
                U.id_usuario,
                U.nombre as "nombre_usuario",
                U.contrasenia,
                U.tipo_usuario,
                D.id_doc_ident,
                T.id_tipo_doc_ident,
                T.nombre as "nombre_tipo",
                T.descripcion as "descripcion_tipo",
                T.cantidad_digitos as "cantidad_digitos_tipo",
                D.valor as "valor_doc_ident",
                A.cant_reclamos_realizados,
                A.cant_reclamos_rechazados,
                A.cant_pedidos_realizados,
                A.cant_pedidos_rechazados
            FROM area A
            INNER JOIN empresa E
            ON A.id_empresa = E.id_empresa
            INNER JOIN usuario U
            ON A.id_area = U.id_usuario
            INNER JOIN documento_identificacion D
            ON U.id_doc_ident = D.id_doc_ident
            INNER JOIN tipo_documento_identificacion T
            ON D.id_tipo_doc_ident = T.id_tipo_doc_ident;    
        `);
        const areas = []
        for(const row of res.rows){
            areas.push({
                id_area: row.id_area,
                nombre: row.nombre_area,
                empresa: {
                    id_empresa: row.id_empresa,
                    nombre: row.nombre_empresa
                },
                usuario: {
                    id_usuario: row.id_usuario,
                    nombre: row.nombre_usuario,
                    contrasenia: row.contrasenia,
                    tipo_usuario: row.tipo_usuario,
                    doc_ident: {
                        id_doc_ident: row.id_doc_ident,
                        tipo_doc_ident: {
                            id_tipo_doc_ident: row.id_tipo_doc_ident,
                            nombre: row.nombre_tipo,
                            descripcion: row.descripcion_tipo,
                            cantidad_digitos: row.cantidad_digitos_tipo 
                        },
                        valor: row.valor_doc_ident,
                    },
                    contactos: await contacto_controller.get_contactos_user(row.id_usuario, client)
                },
                cant_reclamos_realizados: row.cant_reclamos_realizados,
	            cant_reclamos_rechazados: row.cant_reclamos_rechazados,
	            cant_pedidos_realizados: row.cant_pedidos_realizados,
	            cant_pedidos_rechazados: row.cant_pedidos_rechazados,
                partidas: await pp_controller.get_partidas_presupuestaria_area(row.id_area,client)
            })
        }
        return areas;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
const get_area = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT
                A.id_area,
                A.nombre as "nombre_area",
                E.id_empresa,
                E.nombre as "nombre_empresa",
                U.id_usuario,
                U.nombre as "nombre_usuario",
                U.contrasenia,
                U.tipo_usuario,
                D.id_doc_ident,
                T.id_tipo_doc_ident,
                T.nombre as "nombre_tipo",
                T.descripcion as "descripcion_tipo",
                T.cantidad_digitos as "cantidad_digitos_tipo",
                D.valor as "valor_doc_ident",
                A.cant_reclamos_realizados,
                A.cant_reclamos_rechazados,
                A.cant_pedidos_realizados,
                A.cant_pedidos_rechazados
            FROM area A
            INNER JOIN empresa E
            ON A.id_empresa = E.id_empresa
            INNER JOIN usuario U
            ON A.id_area = U.id_usuario
            INNER JOIN documento_identificacion D
            ON U.id_doc_ident = D.id_doc_ident
            INNER JOIN tipo_documento_identificacion T
            ON D.id_tipo_doc_ident = T.id_tipo_doc_ident 
            WHERE A.id_area='${args.id_area}';    
        `);
        const areas = []
        for(const row of res.rows){
            areas.push({
                id_area: row.id_area,
                nombre: row.nombre_area,
                empresa: {
                    id_empresa: row.id_empresa,
                    nombre: row.nombre_empresa
                },
                usuario: {
                    id_usuario: row.id_usuario,
                    nombre: row.nombre_usuario,
                    contrasenia: row.contrasenia,
                    tipo_usuario: row.tipo_usuario,
                    doc_ident: {
                        id_doc_ident: row.id_doc_ident,
                        tipo_doc_ident: {
                            id_tipo_doc_ident: row.id_tipo_doc_ident,
                            nombre: row.nombre_tipo,
                            descripcion: row.descripcion_tipo,
                            cantidad_digitos: row.cantidad_digitos_tipo 
                        },
                        valor: row.valor_doc_ident,
                    },
                    contactos: await contacto_controller.get_contactos_user(row.id_usuario, client)
                },
                cant_reclamos_realizados: row.cant_reclamos_realizados,
	            cant_reclamos_rechazados: row.cant_reclamos_rechazados,
	            cant_pedidos_realizados: row.cant_pedidos_realizados,
	            cant_pedidos_rechazados: row.cant_pedidos_rechazados,
                partidas: await pp_controller.get_partidas_presupuestaria_area(row.id_area,client)
            })
        }
        return areas[0];
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
const create_area = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {nombre, id_area, id_empresa} = args.area;
        
        const query = `
            insert into area(id_area, nombre, id_empresa) 
            values ($1, $2, $3)`;
        
        await client.query(query, [id_area, nombre, id_empresa]);
        
        return id_area;

    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_areas,
    get_area,
    create_area
};