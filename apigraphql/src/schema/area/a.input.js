const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const areaInput = new GraphQLInputObjectType({
    name: 'areaInput',
    fields: {
        nombre: { type: GraphQLString, require: true},
        id_area: { type: GraphQLString, require: true },
        id_empresa: {type: GraphQLString, require: true},
        cant_reclamos_realizados: { type: GraphQLInt},
        cant_reclamos_rechazados: { type: GraphQLInt},
        cant_pedidos_realizados: { type: GraphQLInt},
        cant_pedidos_rechazados: { type: GraphQLInt}
    }
})
module.exports = areaInput;