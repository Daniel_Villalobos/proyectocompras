const a_controller = require('./a.controllers');
const a_input = require('./a.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_area: {
        type: GraphQLString,
        args: {
            area: { type: a_input, required: true}
        },
        resolve: a_controller.create_area
    }
}
module.exports = mutation;
