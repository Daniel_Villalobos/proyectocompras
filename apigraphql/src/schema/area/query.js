
const { GraphQLList, GraphQLString } = require('graphql')
const a_controller = require('./a.controllers')
const type_area = require('./type')

const query = {
    areas: {
        type: new GraphQLList(type_area),
        resolve: a_controller.get_areas
    },
    area: {
        type: type_area,
        args: {
            id_area: {type: GraphQLString, require: true}
        },
        resolve: a_controller.get_area
    }
}
module.exports = query;