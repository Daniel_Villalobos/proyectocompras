const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const estado_productoInput = new GraphQLInputObjectType({
    name: 'estado_productoInput',
    fields: {
        nombre: {type: GraphQLString, require: true},
    }
})
module.exports = estado_productoInput;