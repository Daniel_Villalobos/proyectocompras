const get_estado_producto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM estado_producto;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_estado_producto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {nombre} = args.estado_producto;
      
        const query = `
            INSERT INTO estado_producto
            (nombre)
            VALUES($1) RETURNING id_estado`;
        
        const res = await client.query(query, [nombre]);
        const id = res.rows[0]["id_estado"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_estado_producto,
    create_estado_producto
};