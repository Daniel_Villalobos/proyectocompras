const estado_producto = require("./type");
const ep_controller = require('./ep.controller');
const ep_input = require('./ep.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_estado_producto: {
        type: GraphQLString,
        args: {
            estado_producto: { type: ep_input, required: true}
        },
        resolve: ep_controller.create_estado_producto
    }
}
module.exports = mutation;
