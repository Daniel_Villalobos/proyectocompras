const { GraphQLObjectType } = require("graphql");

const mutation_tipo_documento_identificacion = require('./tipo_documento_identificacion/mutation');
const mutation_area = require('./area/mutation');
const mutation_empresa = require('./empresa/e.mutation');
const mutation_estado_producto = require('./estado_producto/mutation');
const mutation_metodo = require('./metodo/mutation');
const mutation_rubro = require('./rubro/mutation');
const mutation_tipo_cuantia = require('./tipo_cuantia/mutation');
const mutation_tipo_nacionalidad = require('./tipo_nacionalidad/mutation');
const mutation_tipo_naturaleza = require('./tipo_naturaleza/mutation');
const mutation_tipo_reclamo = require('./tipo_reclamo/mutation');
const mutation_tipo_urgencia = require('./tipo_urgencia/mutation');
const mutation_unidad_medida = require('./unidad_medida/mutation');
const mutation_estado_cotizacion = require('./estado_cotizacion/mutation');
const mutation_categoria_producto = require('./categoria_producto/mutation');
const mutation_documento_identificacion = require('./documento_identificacion/mutation')
const mutation_usuario = require('./usuario/mutation')
const mutation_proveedor = require('./proveedor/mutation');
const mutation_contacto = require('./contacto/mutation')
const mutation_operario_compra = require('./operario_compra/mutation')
const mutation_pedido = require('./pedido/mutation')
const rootMutationType = new GraphQLObjectType({
    name: 'RootMutationType',
    fields: () => ({
        ...mutation_tipo_documento_identificacion,
        ...mutation_area,
        ...mutation_empresa,
        ...mutation_estado_producto,
        ...mutation_metodo,
        ...mutation_rubro,
        ...mutation_tipo_cuantia,
        ...mutation_tipo_nacionalidad,
        ...mutation_tipo_naturaleza,
        ...mutation_tipo_reclamo,
        ...mutation_tipo_urgencia,
        ...mutation_unidad_medida,
        ...mutation_estado_cotizacion,
        ...mutation_categoria_producto,
        ...mutation_usuario,
        ...mutation_proveedor,
        ...mutation_contacto,
        ...mutation_documento_identificacion,
        ...mutation_operario_compra,
        ...mutation_pedido
    })
})
module.exports = rootMutationType;