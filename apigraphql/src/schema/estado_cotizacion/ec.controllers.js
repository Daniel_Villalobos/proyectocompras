const get_estados_cotizacion = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM estado_cotizacion;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_estado_cotizacion = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {nombre, descripcion} = args.estado_cotizacion;
      
        const query = `
            INSERT INTO estado_cotizacion
            (descripcion)
            VALUES($1) RETURNING id_estado_cotizacion`;
        
        const res = await client.query(query, [descripcion]);
        const id = res.rows[0]["id_estado_cotizacion"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_estados_cotizacion,
    create_estado_cotizacion
};