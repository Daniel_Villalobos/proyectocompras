const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const estado_cotizacionInput= new GraphQLInputObjectType({
    name: 'estado_cotizacionInput',
    fields: {
        descripcion: {type: GraphQLString, require: true},
    }
})
module.exports = estado_cotizacionInput;