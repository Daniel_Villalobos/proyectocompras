const estado_cotizacion = require("./type");
const ec_controller = require('./ec.controllers');
const ec_input = require('./ec.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_estado_cotizacion: {
        type: GraphQLString,
        args: {
            estado_cotizacion: { type: ec_input, required: true}
        },
        resolve: ec_controller.create_estado_cotizacion
    }
}
module.exports = mutation;