const { GraphQLList } = require('graphql')
const ec_controller = require('./ec.controllers')
const type_estado_cotizacion = require('./type')

const query = {
    estado_cotizacion: {
        type: new GraphQLList(type_estado_cotizacion),
        resolve: ec_controller.get_estados_cotizacion
    }
}
module.exports = query;