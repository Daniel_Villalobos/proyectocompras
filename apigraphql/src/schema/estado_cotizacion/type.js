const {GraphQLObjectType, GraphQLInt, GraphQLString } = require('graphql')

const estados_cotizacion = new GraphQLObjectType({
    name: 'estado_cotizacion',
    fields: () => ({
    
        id_estado_cotizacion: { type: GraphQLInt },
        descripcion: { type: GraphQLString },
        
    })
})
module.exports = estados_cotizacion;