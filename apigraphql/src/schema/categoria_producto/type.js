const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const categoria_producto = new GraphQLObjectType({
    name: 'categoria_producto',
    fields: () => ({
        id_cat_prod: { type: GraphQLString },
        familia: { type: require('../familia/type') },
        descripcion_linea: { type: GraphQLString },
    })
})
module.exports = categoria_producto;