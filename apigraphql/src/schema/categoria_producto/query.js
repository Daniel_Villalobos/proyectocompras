const { GraphQLList } = require('graphql')
const cp_controller = require('./cp.controllers')
const type_categoria_producto = require('./type')

const query = {
    categorias_producto: {
        type: new GraphQLList(type_categoria_producto),
        resolve: cp_controller.get_categorias_producto
    }
}
module.exports = query;