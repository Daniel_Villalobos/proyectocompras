const cp_controller = require('./cp.controllers');
const cp_input = require('./cp.input');
const {  GraphQLString } = require("graphql");
const mutation = {
    create_categoria_producto: {
        type: GraphQLString,
        args: {
            categoria_producto: { type: cp_input, required: true}
        },
        resolve: cp_controller.create_categoria_producto
    }
}
module.exports = mutation;