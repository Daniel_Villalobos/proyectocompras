const get_categorias_producto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT
                CP.id_cat_prod,
                CP.descripcion_linea,
                F.id_familia,
                F.descripcion_familia,
                R.id_rubro,
                R.descripcion_rubro
                FROM categoria_producto CP
                INNER JOIN familia F
                ON CP.id_familia = F.id_familia
                INNER JOIN rubro R
                ON F.id_rubro = R.id_rubro;
        `);
        const categorias_producto = []
        for(const row of res.rows){
            categorias_producto.push({
                id_cat_prod: row.id_cat_prod,
                descripcion_linea: row.descripcion_linea,
                familia: {
                    id_familia: row.id_familia,
                    rubro: {
                        id_rubro: row.id_rubro,
                        descripcion_rubro: row.descripcion_rubro
                    },
                    descripcion_familia: row.descripcion_familia
                }
            })
        }
        return categorias_producto;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_categoria_producto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {id_familia, descripcion_linea} = args.categoria_producto;
        
        const query = `
        insert into categoria_producto (id_familia, descripcion_linea) 
        values ($1, $2) RETURNING id_cat_prod `;
        
        const res = await client.query(query, [id_familia, descripcion_linea]);
    
        const id = res.rows[0]["id_cat_prod"]
        return id;

    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_categorias_producto,
    create_categoria_producto
};