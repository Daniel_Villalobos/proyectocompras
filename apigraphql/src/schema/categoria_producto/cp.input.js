const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const categoria_productoInput = new GraphQLInputObjectType({
    name: 'categoria_productoInput',
    fields: {
        id_familia: { type: GraphQLString, require: true },
        descripcion_linea: { type: GraphQLString, require: true }
    }
})
module.exports = categoria_productoInput;