const {GraphQLObjectType, GraphQLString } = require('graphql')

const rubro = new GraphQLObjectType({
    name: 'rubro',
    fields: () => ({
        id_rubro: { type: GraphQLString },
        descripcion_rubro: { type: GraphQLString }
    })
})
module.exports = rubro;