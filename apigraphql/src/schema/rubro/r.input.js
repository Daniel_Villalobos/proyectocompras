const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const rubroInput = new GraphQLInputObjectType({
    name: 'rubroInput',
    fields: {
        descripcion: {type: GraphQLString, require: true},
    }
})
module.exports = rubroInput;