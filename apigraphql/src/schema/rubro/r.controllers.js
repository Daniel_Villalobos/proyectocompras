const get_rubros = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM rubro;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_rubro = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {descripcion} = args.rubro;
      
        const query = `
            INSERT INTO rubro
            (descripcion)
            VALUES($1) RETURNING id_rubro`;
        
        const res = await client.query(query, [descripcion]);
        const id = res.rows[0]["id_rubro"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}


module.exports = {
    get_rubros,
    create_rubro
};