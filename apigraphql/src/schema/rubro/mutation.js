const rubro = require("./type");
const r_controller = require('./r.controllers');
const r_input = require('./r.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_rubro: {
        type: GraphQLString,
        args: {
            rubro: { type: r_input, required: true}
        },
        resolve: r_controller.create_rubro
    }
}
module.exports = mutation;
