const {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLScalarType } = require('graphql')

const tipo_nacionalidad = new GraphQLObjectType({
    name: 'tipo_nacionalidad',
    fields: () => ({
        id_tipo_nac: { type: GraphQLInt },
        descripcion_tipo_nac: { type: GraphQLString }
    })
})
module.exports = tipo_nacionalidad;