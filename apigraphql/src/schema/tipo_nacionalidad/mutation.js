const tipo_nacionalidad = require("./type");
const tn_controller = require('./tnac.controllers');
const tn_input = require('./tn.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_tipo_nacionalidad: {
        type: GraphQLString,
        args: {
            tipo_nacionalidad: { type: tn_input, required: true}
        },
        resolve: tn_controller.create_tipo_nacionalidad
    }
}
module.exports = mutation;