const get_tipo_nacionalidad = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM tipo_nacionalidad;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_tipo_nacionalidad = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {descripcion_tipo_nac} = args.tipo_nacionalidad;
      
        const query = `
            INSERT INTO tipo_nacionalidad
            (descripcion_tipo_nac)
            VALUES($1) RETURNING id_tipo_nac`;
        
        const res = await client.query(query, [descripcion_tipo_nac]);
        const id = res.rows[0]["id_tipo_nac"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}


module.exports = {
    get_tipo_nacionalidad,
    create_tipo_nacionalidad
};