const { GraphQLList } = require('graphql')
const tnac_controller = require('./tnac.controllers')
const type_tipo_nacionalidad = require('./type')

const query = {
    tipo_nacionalidad: {
        type: new GraphQLList(type_tipo_nacionalidad),
        resolve: tnac_controller.get_tipo_nacionalidad
    }
}
module.exports = query;