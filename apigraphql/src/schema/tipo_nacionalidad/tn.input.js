const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const tipo_nacionalidadInput = new GraphQLInputObjectType({
    name: 'tipo_nacionalidadInput',
    fields: {
        descripcion_tipo_nac: { type: GraphQLString, require: true}
    }
})
module.exports = tipo_nacionalidadInput;