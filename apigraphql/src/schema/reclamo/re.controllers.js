const get_reclamos = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT
                R.id_orden_compra,
                TR.id_tipo_reclamo ,
                TR.descripcion,
                R.descripcion_reclamo ,
                R.respuesta ,
                R.fecha_reclamo
            FROM reclamo R
            INNER JOIN tipo_reclamo TR
            ON TR.id_tipo_reclamo=R.id_tipo_reclamo;
            
            
        `);
        
        const reclamos = []
        for(const row of res.rows){
            reclamo.push({
                id_orden_compra:row.id_orden_compra,
                tipo_reclamo:{
                    id_tipo_reclamo:row.id_tipo_reclamo,
                    descripcion:row.descripcion
                },
                descripcion_reclamo:row.descripcion_reclamo,
                respuesta:row.respuesta,
                fecha_reclamo:row.fecha_reclamo

            }) 


                  
                
            
            
        }
        return reclamos
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
module.exports = {
    get_reclamos
};