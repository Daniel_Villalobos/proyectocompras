const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const reclamos = new GraphQLObjectType({
    name: 'reclamo',
    fields: () => ({
        id_orden_compra: { type: GraphQLInt },
        tipo_reclamo: { type: require('../tipo_reclamo/type') },
        descripcion_reclamo: { type: GraphQLString },
        respuesta: { type: GraphQLString },
        fecha_reclamo: { type: GraphQLString }
        
    })
})
module.exports = reclamos;