const { GraphQLList } = require('graphql')
const re_controller = require('./re.controllers')
const type_reclamo = require('./type')

const query = {
    reclamo: {
        type: new GraphQLList(type_reclamo),
        resolve: re_controller.get_reclamos
    }
}
module.exports = query;