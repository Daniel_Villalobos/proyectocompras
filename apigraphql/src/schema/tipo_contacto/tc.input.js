const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const tipo_contactoInput = new GraphQLInputObjectType({
    name: 'tipo_contactoInput',
    fields: {
        nombre: {type: GraphQLString, require: true},
        descripcion: {type: GraphQLString, require: true}
    }
})
module.exports = tipo_contactoInput;