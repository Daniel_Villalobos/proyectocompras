const tipo_contacto = require("./type");
const tc_controller = require('./tc.controllers');
const tc_input = require('./tc.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_tipo_contacto: {
        type: GraphQLString,
        args: {
            tipo_contacto: { type: tc_input, required: true}
        },
        resolve: tc_controller.create_tipo_contacto
    }
}
module.exports = mutation;