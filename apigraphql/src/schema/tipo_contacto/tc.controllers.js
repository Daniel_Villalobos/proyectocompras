const get_tipos_contacto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM tipo_contacto;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_tipo_contacto = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {nombre, descripcion} = args.tipo_contacto;
      
        const query = `
            INSERT INTO tipo_contacto
            (nombre, descripcion)
            VALUES($1, $2) RETURNING id_tipo_contacto`;
        
        const res = await client.query(query, [nombre, descripcion]);
        const id = res.rows[0]["id_tipo_contacto"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_tipos_contacto,
    create_tipo_contacto
};