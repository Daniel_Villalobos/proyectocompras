const { GraphQLList } = require('graphql')
const tc_controllers = require('./tc.controllers')
const type_tipo_compra = require('./type')

const query = {
    tipo_compra: {
        type: new GraphQLList(type_tipo_compra),
        resolve: tc_controllers.get_tipo_compra
    }
}
module.exports = query;