const tipo_urgencia = require("../tipo_urgencia/tu.controllers");

const get_tipo_compra = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
        SELECT 
            T.id_tipo_compra,
            C.id_tipo_cuantia,
            C.limite_inferior,
            C.limite_superior,
            C.descripcion_tipo_cuantia as "descripcion_tipo",
            N.id_tipo_naturaleza,
            N.descripcion_tipo_naturaleza as "descripcion_tipo",
            U.id_tipo_urg,
            U.descripcion_tipo_urg as "descripcion_tipo",
            T.fecha_creacion
        FROM tipo_compra T
        INNER JOIN tipo_cuantia C
        ON T.id_tipo_cuantia = C.tipo_cuantia
        INNER JOIN tipo_naturaleza N
        ON T.id_tipo_naturaleza = N.id_tipo_naturaleza
        INNER JOIN tipo_urgencia U
        ON T.id_tipo_urg = U.id_tipo_urg; 
        
    `);
        const tipo_compra = []
        for(const row of res.rows){
            tipo_compra.push({
                id_tipo_compra: row.id_tipo_compra,
                tipo_cuantia:{
                    id_tipo_cuantia: row.id_tipo_cuantia,
	                limite_inferior: row.limite_inferior,
	                limite_superior: row.limite_superior,
	                descripcion_tipo_cuantia: row.descripcion_tipo_cuantia,
                }, 
                tipo_naturaleza: {
                    id_tipo_naturaleza: row.id_tipo_naturaleza,
	                descripcion_tipo_naturaleza: row.descripcion_tipo_naturaleza,
                },
                tipo_nacionalidad: {
                    id_tipo_nac: row.id_tipo_nac,
	                descripcion_tipo_nac: row.descripcion_tipo_nac,
                },
                tipo_urgente: {
                    id_tipo_urg: row.id_tipo_urg,
                    descripcion_tipo_urg: row.descripcion_tipo_urg,
                },
                fecha_creacion: row.fecha_creacion,
            })
        }
        return tipo_compra;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

module.exports = {
    get_tipo_compra
};