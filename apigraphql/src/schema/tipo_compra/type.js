const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const tipo_compra = new GraphQLObjectType({
    name: 'tipo_compra',
    fields: () => ({
        id_tipo_compra: { type: GraphQLInt },
        fecha_creacion: { type: GraphQLString },
        tipo_cuantia: {type: require("../tipo_cuantia/type")},
        tipo_naturaleza: {type: require("../tipo_naturaleza/type")},
        tipo_nac: {type: require("../tipo_nacionalidad/type")},
        tipo_urgencia: {type: require("../tipo_urgencia/type")}

    })
})
module.exports = tipo_compra;