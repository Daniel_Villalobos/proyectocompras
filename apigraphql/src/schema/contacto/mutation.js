const c_controller = require('./c.controllers');
const c_input = require('./c.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_contacto: {
        type: GraphQLString,
        args: {
            contacto: { type: c_input, required: true}
        },
        resolve: c_controller.create_contacto
    }
}
module.exports = mutation;