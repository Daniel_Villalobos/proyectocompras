const { GraphQLInputObjectType, GraphQLString } = require("graphql");

const contactoInput = new GraphQLInputObjectType({
    name: 'contactoInput',
    fields: {
        id_usuario: { type: GraphQLString , require: true},
        id_tipo_contacto: { type: GraphQLString, require: true },
        valor: { type: GraphQLString, require: true },
    }
})
module.exports = contactoInput;