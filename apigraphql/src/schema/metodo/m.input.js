const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const metodoInput = new GraphQLInputObjectType({
    name: 'metodoInput',
    fields: {
        id_metodo: { type: GraphQLInt, require: true},
        descripcion_metodo: { type: GraphQLString, require: true}
    }
})
module.exports = metodoInput;