const metodo = require("./type");
const m_controller = require('./m.controllers');
const m_input = require('./m.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_metodo: {
        type: GraphQLString,
        args: {
            metodo: { type: m_input, required: true}
        },
        resolve: m_controller.create_metodo
    }
}
module.exports = mutation;