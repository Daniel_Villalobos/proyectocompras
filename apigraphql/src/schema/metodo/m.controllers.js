const get_metodos = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM metodo
        `);
        
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_metodo = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {id_metodo, descripcion_metodo} = args.metodo;
      
        const query = `
            INSERT INTO metodo
            (id_metodo, descripcion_metodo)
            VALUES($1, $2) RETURNING id_metodo`;
        
        const res = await client.query(query, [id_metodo, descripcion_metodo]);
        const id = res.rows[0]["id_metodo"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_metodos,
    create_metodo
};