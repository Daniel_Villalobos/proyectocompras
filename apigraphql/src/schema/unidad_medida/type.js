const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const unidad_medida = new GraphQLObjectType({
    name: 'unidad_medida',
    fields: () => ({
        id_unidad_medida: { type:  GraphQLInt},
        nombre: { type: GraphQLString },
        descripcion: { type: GraphQLString },
    })
})
module.exports = unidad_medida;