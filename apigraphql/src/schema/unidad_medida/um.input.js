const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const unidad_medidaInput = new GraphQLInputObjectType({
    name: 'unidad_medidaInput',
    fields: {
        nombre: {type: GraphQLString, require: true},
        descripcion: {type: GraphQLString, require: true},
    }
})
module.exports = unidad_medidaInput;