const um_controller = require('./um.controllers');
const um_input = require('./um.input');
const {GraphQLString } = require("graphql");
const mutation = {
    create_unidad_medida: {
        type: GraphQLString,
        args: {
            unidad_medida: { type: um_input, required: true}
        },
        resolve: um_controller.create_unidad_medida
    }
}
module.exports = mutation;