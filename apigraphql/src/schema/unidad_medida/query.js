const { GraphQLList } = require('graphql')
const um_controller = require('./um.controllers')
const type = require('./type')

const query = {
    unidades_medida: {
        type: new GraphQLList(type),
        resolve: um_controller.get_unidades_medidas
    }
}
module.exports = query;