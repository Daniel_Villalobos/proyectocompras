const get_unidades_medidas = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM unidad_medida;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_unidad_medida = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {nombre, descripcion} = args.unidad_medida;
      
        const query = `
            INSERT INTO unidad_medida
            (nombre, descripcion)
            VALUES($1, $2) RETURNING id_unidad_medida`;
        
        const res = await client.query(query, [nombre, descripcion]);
        const id = res.rows[0]["id_unidad_medida"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_unidades_medidas,
    create_unidad_medida
};