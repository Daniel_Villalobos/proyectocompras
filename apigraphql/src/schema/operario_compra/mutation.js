const oc_controller = require('./oc.controllers');
const oc_input = require('./oc.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_operario: {
        type: GraphQLString,
        args: {
            operario: { type: oc_input, required: true}
        },
        resolve: oc_controller.create_operario
    }
}
module.exports = mutation;