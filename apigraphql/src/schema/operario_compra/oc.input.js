const { GraphQLInputObjectType, GraphQLString } = require("graphql")

const operarioInput = new GraphQLInputObjectType({
    name: 'operarioInput',
    fields: {
        id_operario: { type: GraphQLString, require :true  },
        id_rol_operario: { type: GraphQLString, require :true  },
    }
})
module.exports = operarioInput;