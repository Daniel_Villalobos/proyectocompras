const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const objeto_gasto = new GraphQLObjectType({
    name: 'objeto_gasto',
    fields: () => ({
        id_obj_gasto: { type: GraphQLInt },
        descripcion: { type: GraphQLString }
    })
})
module.exports = objeto_gasto;