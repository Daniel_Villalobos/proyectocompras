const { GraphQLList } = require('graphql')
const og_controller = require('./og.controllers')
const type_objeto_gasto = require('./type')

const query = {
    objetos_gasto: {
        type: new GraphQLList(type_objeto_gasto),
        resolve: og_controller.get_objeto_gasto
    }
}
module.exports = query;