const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const protocolo_transaccion = new GraphQLObjectType({
    name: 'protocolo_transaccion',
    fields: () => ({
        id_protocolo: { type: GraphQLInt },
        id_transac: { type: GraphQLInt },
        secuencia: { type: GraphQLInt },
    })
})
module.exports = protocolo_transaccion;