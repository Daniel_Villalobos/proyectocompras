const get_protocolo_transaccion = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM protocolo_transaccion;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

module.exports = {
    get_protocolo_transaccion
};