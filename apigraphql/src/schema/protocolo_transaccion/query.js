const { GraphQLList } = require('graphql')
const pt_controllers = require('./pt.controllers')
const type_protocolo_transaccion= require('./type')

const query = {
    periodo_disponible_prov: {
        type: new GraphQLList(type_protocolo_transaccion),
        resolve: pt_controllers.get_protocolo_transaccion
    }
}
module.exports = query;