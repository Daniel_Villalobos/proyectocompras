const get_aprobacion = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT 
                A.id_pedido,
                A.id_operario,
                EA.id_estado_aprob,
                EA.descripcion
            FROM aprobacion A
            INNER JOIN estado_aprobacion EA
            ON EA.id_estado_aprob=A.id_estado_aprob;
        `);
        const aprobaciones=[]
            for(const row of res.rows){
                aprobaciones.push({
                    id_pedido:row.id_pedido,
                    id_operario:row.id_operario,
                    estado_aprob:{
                        id_estado_aprob:row.id_estado_aprob,
                        descripcion:row.descripcion
                    }

                })
            }
        return aprobaciones
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
const get_aprobaciones_operario=async (id_operario,client) => {
    
    try{
        const res = await client.query(`
            SELECT 
                A.id_pedido,
                A.id_operario,
                EA.id_estado_aprob,
                EA.descripcion
            FROM aprobacion A
            INNER JOIN estado_aprobacion EA
            ON EA.id_estado_aprob=A.id_estado_aprob
            WHERE A.id_operario=${id_operario};   
        `);
        const aprobaciones=[]
            for(const row of res.rows){
                aprobaciones.push({
                    id_pedido:row.id_pedido,
                    id_operario:row.id_operario,
                    estado_aprob:{
                        id_estado_aprob:row.id_estado_aprob,
                        descripcion:row.descripcion
                    }

                })
            }
        return aprobaciones
        
                    
    }catch(e){
        console.log(e)
    }
}
module.exports = {
    get_aprobacion,
    get_aprobaciones_operario

};