const {GraphQLObjectType, GraphQLString } = require('graphql')

const aprobaciones = new GraphQLObjectType({
    name: 'aprobacion',
    fields: () => ({
        id_pedido: { type: GraphQLString },
        id_operario: { type: GraphQLString },
        estado_aprob: { type: require('../estado_aprobacion/type') },
        
    })
})
module.exports = aprobaciones;