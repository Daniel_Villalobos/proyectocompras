const { GraphQLList } = require('graphql')
const ap_controller = require('./ap.controllers')
const type_aprobacion = require('./type')

const query = {
    aprobacion: {
        type: new GraphQLList(type_aprobacion),
        resolve: ap_controller.get_aprobacion
    }
}
module.exports = query;