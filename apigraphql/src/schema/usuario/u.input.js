const { GraphQLInputObjectType, GraphQLString } = require("graphql");

const usuarioInput = new GraphQLInputObjectType({
    name: 'usuarioInput',
    fields: {
        nombre: { type: GraphQLString , require: true},
        contrasenia: { type: GraphQLString, require: true },
        tipo_usuario: { type: GraphQLString, require: true},
        id_doc_ident: { type: GraphQLString, require: true },
    }
})
module.exports = usuarioInput;