const {GraphQLObjectType, GraphQLString, GraphQLList } = require('graphql')

const usuario = new GraphQLObjectType({
    name: 'usuario',
    fields: () => ({
        id_usuario: { type: GraphQLString },
        nombre: { type: GraphQLString },
        contrasenia: { type: GraphQLString },
        tipo_usuario: { type: GraphQLString },
        doc_ident: { type: require("../documento_identificacion/type") },
        contactos: { type: GraphQLList(require("../contacto/type"))}
    })
})
module.exports = usuario;