const u_controller = require('./u.controllers');
const u_input = require('./u.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_usuario: {
        type: GraphQLString,
        args: {
            usuario: { type: u_input, required: true}
        },
        resolve: u_controller.create_usuario
    }
}
module.exports = mutation;