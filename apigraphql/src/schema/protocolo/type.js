const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql');

const protocolo = new GraphQLObjectType({
    name: 'protocolo',
    fields: () => ({
        id_protocolo: { type: GraphQLInt },
        tipo_compra: {type: require("../tipo_compra/type")},
        descripcion: {type: GraphQLString},
            
    })
})
module.exports = protocolo;