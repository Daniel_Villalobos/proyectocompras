const get_protocolos = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT 
                P.id_protocolo,
                T.id_tipo_compra,
                C.id_tipo_cuantia,
                C.limite_inferior,
                C.limite_superior,
                C.descripcion_tipo_cuantia,
                N.id_tipo_naturaleza,
                N.descripcion_tipo_naturaleza,
                U.id_tipo_urg,
                U.descripcion_tipo_urg,
                T.fecha_creacion,
                P.descripcion
            FROM protocolo P
            INNER JOIN tipo_compra T
            ON P.id_tipo_compra = T.id_tipo_compra
            INNER JOIN tipo_cuantia C
            ON T.id_tipo_cuantia = C.tipo_cuantia
            INNER JOIN tipo_naturaleza N
            ON T.id_tipo_naturaleza = N.id_tipo_naturaleza
            INNER JOIN tipo_urgencia U
            ON T.id_tipo_urg = U.id_tipo_urg;
        `);
        const protocolo = []
        for(const row of res.rows){
            protocolo.push({
                id_protocolo: row.id_protocolo,
                tipo_compra:{
                    id_tipo_compra: row.id_tipo_compra,
                    tipo_cuantia:{
                        id_tipo_cuantia: row.id_tipo_cuantia,
	                    limite_inferior: row.limite_inferior,
	                    limite_superior: row.limite_superior,
	                    descripcion_tipo_cuantia: row.descripcion_tipo_cuantia,
                    },
                    
                    tipo_naturaleza:{
                        id_tipo_naturaleza: row.id_tipo_naturaleza,
	                    descripcion_tipo_naturaleza: row.descripcion_tipo_naturaleza,
                    },

                    tipo_nacionalidad:{
                        id_tipo_nac: row.id_tipo_nac,
	                    descripcion_tipo_nac: row.descripcion_tipo_nac,
                    },

                    tipo_urgencia:{
                        id_tipo_urg: row.id_tipo_urg,
                        descripcion_tipo_urg: row.descripcion_tipo_urg,
                    },
                    
                    fecha_creacion: row.fecha_creacion,
                },
              
                descripcion: row.descripcion
            })
        }
        return familias;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

module.exports = {
    get_protocolos
};