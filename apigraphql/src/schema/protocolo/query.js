const { GraphQLList } = require('graphql')
const p_controllers = require('./p.controllers')
const type_protocolo= require('./type')

const query = {
    protocolo: {
        type: new GraphQLList(type_protocolo),
        resolve: p_controllers.get_protocolo
    }
}
module.exports = query;