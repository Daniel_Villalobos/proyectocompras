const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const empresaInput = new GraphQLInputObjectType({
    name: 'empresaInput',
    fields: {
        nombre: {type: GraphQLString, require: true},
    }
})
module.exports = empresaInput;
