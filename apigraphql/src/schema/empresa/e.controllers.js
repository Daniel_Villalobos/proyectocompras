const get_empresas = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM empresa;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_empresa = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {nombre} = args.empresa;
      
        const query = `
            INSERT INTO empresa
            (nombre)
            VALUES($1) RETURNING id_empresa`;
        
        const res = await client.query(query, [nombre]);
        const id = res.rows[0]["id_empresa"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_empresas,
    create_empresa
};