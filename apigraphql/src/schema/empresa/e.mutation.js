const empresa = require("./type");
const e_controller = require('./e.controllers');
const e_input = require('./e.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_empresa: {
        type: GraphQLString,
        args: {
            empresa: { type: e_input, required: true}
        },
        resolve: e_controller.create_empresa
    }
}
module.exports = mutation;