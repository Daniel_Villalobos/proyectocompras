const tipo_reclamo = require("./type");
const tr_controller = require('./tr.controllers');
const tr_input = require('./tr.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_tipo_reclamo: {
        type: GraphQLString,
        args: {
            tipo_reclamo: { type: tr_input, required: true}
        },
        resolve: tr_controller.create_tipo_reclamo
    }
}
module.exports = mutation;