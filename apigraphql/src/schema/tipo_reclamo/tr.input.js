const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const tipo_reclamoInput = new GraphQLInputObjectType({
    name: 'tipo_reclamoInput',
    fields: {
        descripcion: { type: GraphQLString, require: true}
    }
})
module.exports = tipo_reclamoInput;