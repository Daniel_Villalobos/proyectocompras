const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const tipos_reclamo = new GraphQLObjectType({
    name: 'tipo_reclamo',
    fields: () => ({
        id_tipo_reclamo: { type: GraphQLInt },
        descripcion: { type: GraphQLString },
        
    })
})
module.exports = tipos_reclamo;