const { GraphQLList } = require('graphql')
const tr_controller = require('./tr.controllers')
const type_tipo_reclamo = require('./type')

const query = {
    tipo_reclamo: {
        type: new GraphQLList(type_tipo_reclamo),
        resolve: tr_controller.get_tipos_reclamo
    }
}
module.exports = query;