const get_tipos_reclamo = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM tipo_reclamo;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_tipo_reclamo = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {descripcion} = args.tipo_reclamo;
      
        const query = `
            INSERT INTO tipo_reclamo
            (descripcion)
            VALUES($1) RETURNING id_tipo_reclamo`;
        
        const res = await client.query(query, [descripcion]);
        const id = res.rows[0]["id_tipo_reclamo"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_tipos_reclamo,
    create_tipo_reclamo
};