const get_orden_de_compra = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT
                OC.id_orden_compra,
                OC.id_proveedor,
                OC.id_operario,
                Req.id_pedido,
                Req.nro_requerimiento,
                OC.fecha_envio_orden
            FROM orden_de_compra OC
            INNER JOIN requerimiento Req
            ON Req.id_pedido=OC.id_pedido;
           
        `);
        const ordenes = []
        for(const row of res.rows){
            ordenes.push({
                id_orden_compra:row.id_orden_compra,
                id_proveedor:row.id_proveedor,
                id_operario:row.id_operario,
                requerimiento:{
                    id_pedido:row.id_pedido,
                    nro_requerimiento:row.nro_requerimiento

                },
                fecha_envio_orden:row.fecha_envio_orden

            }) 
        }
        return ordenes
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}
module.exports = {
    get_orden_de_compra
};