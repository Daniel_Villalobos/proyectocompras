const { GraphQLList } = require('graphql')
const oc_controller = require('./odc.controllers')
const type_orden_de_compra = require('./type')

const query = {
    ordenes_compra: {
        type: new GraphQLList(type_orden_de_compra),
        resolve: oc_controller.get_orden_de_compra
    }
}
module.exports = query;