const {GraphQLObjectType, GraphQLInt, GraphQLString } = require('graphql')

const orden_de_compra = new GraphQLObjectType({
    name: 'orden_de_compra',
    fields: () => ({
    
        id_orden_compra: { type: GraphQLInt },
        id_proveedor:  { type: GraphQLInt },
        id_operario:  { type: GraphQLInt },
        
        nro_requerimiento: { type: require("../requerimiento/type") },
        pedido: { type: require("../requerimiento/type") },
        fecha_envio_orden: { type: GraphQLString },
    })
})
module.exports = orden_de_compra;