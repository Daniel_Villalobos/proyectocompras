const odc_controller = require('./odc.controllers');
const odc_input = require('./odc.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_orden_de_compra: {
        type: GraphQLString,
        args: {
            orden_compra: { type: odc_input, required: true}
        },
        resolve: odc_controller.create_orden_de_compra
    }
}
module.exports = mutation;