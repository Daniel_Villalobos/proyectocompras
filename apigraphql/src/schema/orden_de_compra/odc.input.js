const { GraphQLInputObjectType, GraphQLString } = require("graphql")

const orden_de_compra = new GraphQLInputObjectType({
    name: 'orden_de_compraInput',
    fields: {
        id_proveedor: { type: GraphQLString, require :true  },
        id_operario: { type: GraphQLString, require :true  },
        id_pedido: { type: GraphQLString, require :true  },
        nro_requerimiento: { type: GraphQLString, require :true  }
    }
})
module.exports = orden_de_compra;
/* insert into orden_de_compra(id_proveedor, id_operario, id_pedido, nro_requerimiento)
values ($1, $2, $3, $4); */