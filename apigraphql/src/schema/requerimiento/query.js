const { GraphQLList, GraphQLString } = require('graphql')
const req_controller = require('./req.controllers')
const type_requerimiento = require('./type')

const query = {
    requerimientos: {
        type: new GraphQLList(type_requerimiento),
        resolve: req_controller.get_requerimientos
    },
    requerimiento: {
        type: type_requerimiento,
        args: {
            id_pedido: {type: GraphQLString}
        },
        resolve: req_controller.get_requerimiento
    }
}
module.exports = query;