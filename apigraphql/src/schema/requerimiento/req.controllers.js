const get_requerimientos = async (parent, args, context, info) => {
  const client = await context.db.connect();
  try {
    const res = await client.query(`
            SELECT
                Req.nro_requerimiento,
                Req.id_pedido,
                Req.cantidad,
                TP.id_tipo_prod,
                EP.id_estado,
                EP.nombre as "nombre_estado",
                CP.id_cat_prod,
                CP.descripcion_linea,
                F.id_familia,
                F.descripcion_familia,
                R.id_rubro,
                R.descripcion_rubro,
                TP.nombre_prod,
                TP.precio_ref,
                TP.descripcion_prod,
                TP.antiguedad,
                UM.id_unidad_medida,
                UM.nombre,
                UM.descripcion as "descripcion_um",
                Req.fecha_min,
                Req.fecha_max
            FROM requerimiento Req
            INNER JOIN tipo_producto TP
            ON TP.id_tipo_prod=Req.id_tipo_prod
            INNER JOIN estado_producto EP
            ON EP.id_estado = TP.id_estado
            INNER JOIN categoria_producto CP
            ON TP.id_cat_prod = CP.id_cat_prod
            INNER JOIN familia F
            ON CP.id_familia = F.id_familia
            INNER JOIN rubro R
            ON F.id_rubro = R.id_rubro
            INNER JOIN unidad_medida UM
            ON UM.id_unidad_medida=Req.id_unidad_medida; 
        `);

    const requerimientos = [];
    for (const row of res.rows) {
      requerimientos.push({
        nro_requerimiento: row.nro_requerimiento,
        id_pedido: row.id_pedido,
        cantidad: row.cantidad,
        tipo_producto: {
          id_tipo_prod: row.id_tipo_prod,
          estado_producto: {
            id_estado: row.id_estado,
            nombre: row.nombre_estado,
          },
          categoria_producto: {
            id_cat_prod: row.id_cat_prod,
            descripcion_linea: row.descripcion_linea,
            familia: {
              id_familia: row.id_familia,
              rubro: {
                id_rubro: row.id_rubro,
                descripcion_rubro: row.descripcion_rubro,
              },
              descripcion_familia: row.descripcion_familia,
            },
          },
          nombre_prod: row.nombre_prod,
          precio_ref: row.precio_ref,
          descripcion_prod: row.descripcion_prod,
          antiguedad: row.antiguedad,
        },
        unidad_medida: {
          id_unidad_medida: row.id_unidad_medida,
          nombre: row.nombre,
          descripcion: row.descripcion_um
        },
        fecha_min: row.fecha_min,
        fecha_max: row.fecha_max,
      });
    }
    return requerimientos;
  } catch (e) {
    console.log(e);
  } finally {
    client.release();
  }
};
const get_requerimiento = async (parent, args, context, info) => {
  const client = await context.db.connect();
  try {
    const res = await client.query(`
            SELECT
                Req.nro_requerimiento,
                Req.id_pedido,
                Req.cantidad,
                TP.id_tipo_prod,
                EP.id_estado,
                EP.nombre as "nombre_estado",
                CP.id_cat_prod,
                CP.descripcion_linea,
                F.id_familia,
                F.descripcion_familia,
                R.id_rubro,
                R.descripcion_rubro,
                TP.nombre_prod,
                TP.precio_ref,
                TP.descripcion_prod,
                TP.antiguedad,
                UM.id_unidad_medida,
                UM.nombre,
                UM.descripcion as "descripcion_um",
                Req.fecha_min,
                Req.fecha_max
            FROM requerimiento Req
            INNER JOIN tipo_producto TP
            ON TP.id_tipo_prod=Req.id_tipo_prod
            INNER JOIN estado_producto EP
            ON EP.id_estado = TP.id_estado
            INNER JOIN categoria_producto CP
            ON TP.id_cat_prod = CP.id_cat_prod
            INNER JOIN familia F
            ON CP.id_familia = F.id_familia
            INNER JOIN rubro R
            ON F.id_rubro = R.id_rubro
            INNER JOIN unidad_medida UM
            ON UM.id_unidad_medida=Req.id_unidad_medida
            WHERE Req.id_pedido='${args.id_pedido }'; 
        `);

    const requerimientos = [];
    for (const row of res.rows) {
      requerimientos.push({
        nro_requerimiento: row.nro_requerimiento,
        id_pedido: row.id_pedido,
        cantidad: row.cantidad,
        tipo_producto: {
          id_tipo_prod: row.id_tipo_prod,
          estado_producto: {
            id_estado: row.id_estado,
            nombre: row.nombre_estado,
          },
          categoria_producto: {
            id_cat_prod: row.id_cat_prod,
            descripcion_linea: row.descripcion_linea,
            familia: {
              id_familia: row.id_familia,
              rubro: {
                id_rubro: row.id_rubro,
                descripcion_rubro: row.descripcion_rubro,
              },
              descripcion_familia: row.descripcion_familia,
            },
          },
          nombre_prod: row.nombre_prod,
          precio_ref: row.precio_ref,
          descripcion_prod: row.descripcion_prod,
          antiguedad: row.antiguedad,
        },
        unidad_medida: {
          id_unidad_medida: row.id_unidad_medida,
          nombre: row.nombre,
          descripcion: row.descripcion_um
        },
        fecha_min: row.fecha_min,
        fecha_max: row.fecha_max,
      });
    }
    return requerimientos;
  } catch (e) {
    console.log(e);
  } finally {
    client.release();
  }
};
const get_requerimientos_pedido = async (client, id_pedido) => {
  try {
    const res = await client.query(`
            SELECT
                Req.nro_requerimiento,
                Req.id_pedido,
                Req.cantidad,
                TP.id_tipo_prod,
                EP.id_estado,
                EP.nombre as "nombre_estado",
                CP.id_cat_prod,
                CP.descripcion_linea,
                F.id_familia,
                F.descripcion_familia,
                R.id_rubro,
                R.descripcion_rubro,
                TP.nombre_prod,
                TP.precio_ref,
                TP.descripcion_prod,
                TP.antiguedad,
                UM.id_unidad_medida,
                UM.nombre,
                UM.descripcion as "descripcion_um",
                Req.fecha_min,
                Req.fecha_max
            FROM requerimiento Req
            INNER JOIN tipo_producto TP
            ON TP.id_tipo_prod=Req.id_tipo_prod
            INNER JOIN estado_producto EP
            ON EP.id_estado = TP.id_estado
            INNER JOIN categoria_producto CP
            ON TP.id_cat_prod = CP.id_cat_prod
            INNER JOIN familia F
            ON CP.id_familia = F.id_familia
            INNER JOIN rubro R
            ON F.id_rubro = R.id_rubro
            INNER JOIN unidad_medida UM
            ON UM.id_unidad_medida=Req.id_unidad_medida
            WHERE Req.id_pedido = '${id_pedido}'; 
        `);

    const requerimientos = [];
    for (const row of res.rows) {
      requerimientos.push({
        nro_requerimiento: row.nro_requerimiento,
        id_pedido: row.id_pedido,
        cantidad: row.cantidad,
        tipo_producto: {
          id_tipo_prod: row.id_tipo_prod,
          estado_producto: {
            id_estado: row.id_estado,
            nombre: row.nombre_estado,
          },
          categoria_producto: {
            id_cat_prod: row.id_cat_prod,
            descripcion_linea: row.descripcion_linea,
            familia: {
              id_familia: row.id_familia,
              rubro: {
                id_rubro: row.id_rubro,
                descripcion_rubro: row.descripcion_rubro,
              },
              descripcion_familia: row.descripcion_familia,
            },
          },
          nombre_prod: row.nombre_prod,
          precio_ref: row.precio_ref,
          descripcion_prod: row.descripcion_prod,
          antiguedad: row.antiguedad,
        },
        unidad_medida: {
          id_unidad_medida: row.id_unidad_medida,
          nombre: row.nombre,
          descripcion: row.descripcion_um
        },
        fecha_min: row.fecha_min,
        fecha_max: row.fecha_max,
      });
    }
    return requerimientos;
  } catch (e) {
    console.log(e);
  }
};

const create_requerimiento = async (parent, args, context, info) => {
  const client = await context.db.connect()
  try{
      const {nro_requerimiento, id_pedido, cantidad,
         id_tipo_prod, id_unidad_medida} = args.requerimiento;
      
      const query = `
      insert into requerimiento (nro_requerimiento, id_pedido, cantidad,
        id_tipo_prod, id_unidad_medida)
      values ($1, $2, $3, $4, $5);`;
      
      await client.query(query, [nro_requerimiento, id_pedido, cantidad,
        id_tipo_prod, id_unidad_medida]);
      return id_pedido + nro_requerimiento;
  }catch(e){
      console.log(e);
  }finally{
      client.release()
  }
}
module.exports = {
  get_requerimientos,
  get_requerimientos_pedido,
  create_requerimiento
};
