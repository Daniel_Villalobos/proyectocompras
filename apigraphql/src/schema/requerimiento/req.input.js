const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLFloat } = require("graphql")

const requerimientoInput = new GraphQLInputObjectType({
    name: 'requerimientoInput',
    fields: {
        nro_requerimiento: { type: GraphQLString, require :true  },
        id_pedido: { type: GraphQLString, require :true  },
        cantidad: { type: GraphQLInt },
        id_tipo_prod: { type: GraphQLString },
        id_unidad_medida: { type: GraphQLInt }
    }
})
module.exports = requerimientoInput;