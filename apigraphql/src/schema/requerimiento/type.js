const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const requerimiento = new GraphQLObjectType({
    name: 'requerimiento',
    fields: () => ({
        nro_requerimiento: { type: GraphQLInt },
        id_pedido:{ type: GraphQLString },
        cantidad:{ type: GraphQLInt },
        tipo_producto: { type: require('../tipo_producto/type') },
        unidad_medida: { type: require('../unidad_medida/type') },
        fecha_min: { type: GraphQLString },
        fecha_max: { type: GraphQLString },
    })
})
module.exports = requerimiento;