const req_controller = require('./req.controllers');
const req_input = require('./req.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_requerimiento: {
        type: GraphQLString,
        args: {
            requerimiento: { type: req_input, required: true}
        },
        resolve: req_controller.create_requerimiento
    }
}
module.exports = mutation;