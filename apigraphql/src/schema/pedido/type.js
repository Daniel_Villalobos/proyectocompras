const {GraphQLObjectType, GraphQLString, GraphQLList } = require('graphql')

const pedido = new GraphQLObjectType({
    name: 'pedido',
    fields: () => ({
        id_pedido: { type: GraphQLString },
        id_area: { type: GraphQLString },
        fecha_pedido: { type: GraphQLString },
	    fecha_resp_pedido:{ type: GraphQLString },
        requerimientos: {type: GraphQLList(require('../requerimiento/type'))}
    })
})


module.exports = pedido;