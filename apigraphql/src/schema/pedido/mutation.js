const p_controller = require('./p.controllers');
const p_input = require('./p.input');
const {GraphQLString } = require("graphql");
const mutation = {
    create_pedido: {
        type: GraphQLString,
        args: {
            pedido: { type: p_input, required: true}
        },
        resolve: p_controller.create_pedido
    }
}
module.exports = mutation;