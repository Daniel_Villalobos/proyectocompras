const { GraphQLList } = require('graphql')
const p_controller = require('./p.controllers')
const pedido = require('./type')

const query = {
    pedidos: {
        type: new GraphQLList(pedido),
        resolve: p_controller.get_pedidos
    }
}
module.exports = query;