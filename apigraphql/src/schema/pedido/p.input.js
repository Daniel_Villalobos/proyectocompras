const { GraphQLInputObjectType, GraphQLString } = require("graphql");

const pedidoInput = new GraphQLInputObjectType({
    name: 'pedidoInput',
    fields: {
        id_area: {type: GraphQLString, require: true},
    }
})
module.exports = pedidoInput;