const r_controller = require('../requerimiento/req.controllers')
const get_pedidos = async (parent, args, context, info) => {
  const client = await context.db.connect();
  try {
    const res = await client.query(`
           select * from pedido;
        `);
    const pedidos = [];
    for (const row of res.rows) {
      pedidos.push({
        ...row,
        requerimientos: await r_controller.get_requerimientos_pedido(client, row.id_pedido)
      });
    }
    return pedidos;
  } catch (e) {
    console.log(e);
  } finally {
    client.release();
  }
};

const create_pedido = async (parent, args, context, info) => {
  const client = await context.db.connect();
  try {
    const { id_area } = args.pedido;

    const query = `
        insert into pedido(id_area)
        values ($1) returning id_pedido;`;

    const res = await client.query(query, [id_area]);
    const id = res.rows[0]["id_pedido"];

    return id;
  } catch (e) {
    console.log(e);
  } finally {
    client.release();
  }
};

module.exports = {
  get_pedidos,
  create_pedido,
};
