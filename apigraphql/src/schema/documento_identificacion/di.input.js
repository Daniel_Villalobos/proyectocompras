const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const documento_identificacionInput = new GraphQLInputObjectType({
    name: 'documento_identificacionInput',
    fields: {
        id_tipo_doc_ident: { type: GraphQLString , require: true},
        valor: { type: GraphQLString, require: true }
    }
})
module.exports = documento_identificacionInput;