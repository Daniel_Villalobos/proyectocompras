const documento_identificacion = require("./type");
const di_controller = require('./di.controllers');
const di_input = require('./di.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_documento_identificacion: {
        type: GraphQLString,
        args: {
            documento_identificacion: { type: di_input, required: true}
        },
        resolve: di_controller.create_documento_identificacion
    }
}
module.exports = mutation;