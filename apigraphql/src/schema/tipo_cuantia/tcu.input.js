const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const tipo_cuantiaInput = new GraphQLInputObjectType({
    name: 'tipo_cuantiaInput',
    fields: {
        limite_inferior: {type: GraphQLInt, require: true},
        limite_superior: {type: GraphQLInt, require: true},
        descripcion_tipo_cuantia: { type: GraphQLString, require: true}
    }
})
module.exports = tipo_cuantiaInput;