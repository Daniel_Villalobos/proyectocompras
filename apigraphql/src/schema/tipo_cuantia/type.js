const {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLScalarType } = require('graphql')

const tipo_cuantia = new GraphQLObjectType({
    name: 'tipo_cuantia',
    fields: () => ({
        id_tipo_cuantia: { type: GraphQLInt },
        limite_inferior: {type: GraphQLInt},
        limite_superior: {type: GraphQLInt},
        descripcion_tipo_cuantia: { type: GraphQLString }
    })
})
module.exports = tipo_cuantia;