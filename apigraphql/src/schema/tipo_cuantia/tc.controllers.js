const get_tipo_cuantia = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM tipo_cuantia;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_tipo_cuantia = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {limite_inferior, limite_superior, descripcion_tipo_cuantia} = args.tipo_cuantia;
      
        const query = `
            INSERT INTO tipo_cuantia
            (limite_inferior, limite_superior, descripcion_tipo_cuantia)
            VALUES($1, $2, $3) RETURNING id_tipo_cuantia`;
        
        const res = await client.query(query, [limite_inferior, limite_superior, descripcion_tipo_cuantia]);
        const id = res.rows[0]["id_tipo_cuantia"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_tipo_cuantia,
    create_tipo_cuantia
};