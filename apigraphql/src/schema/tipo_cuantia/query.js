const { GraphQLList } = require('graphql')
const tc_controller = require('./tc.controllers')
const type_tipo_cuantia = require('./type')

const query = {
    tipo_cuantia: {
        type: new GraphQLList(type_tipo_cuantia),
        resolve: tc_controller.get_tipo_cuantia
    }
}
module.exports = query;