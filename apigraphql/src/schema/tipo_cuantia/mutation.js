const tipo_cuantia = require("./type");
const tcu_controller = require('./tc.controllers');
const tcu_input = require('./tcu.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_tipo_cuantia: {
        type: GraphQLString,
        args: {
            tipo_cuantia: { type: tcu_input, required: true}
        },
        resolve: tcu_controller.create_tipo_cuantia
    }
}
module.exports = mutation;