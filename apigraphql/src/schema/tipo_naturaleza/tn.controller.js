const get_tipo_naturaleza = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT * FROM tipo_naturaleza;
        `);
        return res.rows;
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

const create_tipo_naturaleza = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const {descripcion_tipo_naturaleza} = args.tipo_naturaleza;
      
        const query = `
            INSERT INTO tipo_naturaleza
            (descripcion_tipo_naturaleza)
            VALUES($1) RETURNING id_tipo_naturaleza`;
        
        const res = await client.query(query, [descripcion_tipo_naturaleza]);
        const id = res.rows[0]["id_tipo_naturaleza"]
        
        return id;
    }catch(e){
        console.log(e);
    }finally{
        client.release()
    }
}

module.exports = {
    get_tipo_naturaleza,
    create_tipo_naturaleza
};