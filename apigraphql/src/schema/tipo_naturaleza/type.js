const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const tipo_naturaleza = new GraphQLObjectType({
    name: 'tipo_naturaleza',
    fields: () => ({
        id_tipo_naturaleza: { type: GraphQLString },
        descripcion_tipo_naturaleza: { type: GraphQLString }
    })
})
module.exports = tipo_naturaleza;