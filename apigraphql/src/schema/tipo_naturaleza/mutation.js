const tipo_naturaleza = require("./type");
const tn_controller = require('./tn.controller');
const tn_input = require('./tn.input');
const { GraphQLObjectType, GraphQLString } = require("graphql");
const mutation = {
    create_tipo_naturaleza: {
        type: GraphQLString,
        args: {
            tipo_naturaleza: { type: tn_input, required: true}
        },
        resolve: tn_controller.create_tipo_naturaleza
    }
}
module.exports = mutation;