const { GraphQLInputObjectType, GraphQLString, GraphQLInt } = require("graphql");

const tipo_naturalezaInput = new GraphQLInputObjectType({
    name: 'tipo_naturalezaInput',
    fields: {
        descripcion_tipo_naturaleza: { type: GraphQLString, require: true}
    }
})
module.exports = tipo_naturalezaInput;