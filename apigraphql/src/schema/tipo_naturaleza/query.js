const { GraphQLList } = require('graphql')
const tn_controller = require('./tn.controller')
const type_tipo_naturaleza = require('./type')

const query = {
    tipo_naturaleza: {
        type: new GraphQLList(type_tipo_naturaleza),
        resolve: tn_controller.get_tipo_naturaleza
    }
}
module.exports = query;