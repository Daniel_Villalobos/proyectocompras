const { GraphQLList } = require('graphql')
const co_controller = require('./co.controllers')
const type_cotizacion = require('./type')

const query = {
    cotizacion: {
        type: new GraphQLList(type_cotizacion),
        resolve: co_controller.get_cotizacion
    }
}
module.exports = query;