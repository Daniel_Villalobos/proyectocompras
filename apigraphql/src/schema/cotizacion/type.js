const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const cotizacion = new GraphQLObjectType({
    name: 'cotizacion',
    fields: () => ({
        id_cot:{ type: GraphQLInt },
        puntaje:{ type: GraphQLInt },
        descuento:{ type: GraphQLInt },
        monto_total:{ type: GraphQLInt },
        fecha_entrega_min:{ type: GraphQLString },
        fecha_entrega_max:{ type: GraphQLString },
        fecha_envio_prov:{ type: GraphQLString },
        fecha_recibo_prov:{ type: GraphQLString },
        fecha_envio_op:{ type: GraphQLString },
        fecha_recibo_op:{ type: GraphQLString },
        id_orden_compra: { type: GraphQLInt },
        estado_cotizacion: { type: require('../estado_cotizacion/type') },
        requerimiento: {type: require('../requerimiento/type')}, //falta requerimeinto y pedido
        nro_requerimiento:{ type: GraphQLInt },
        id_proveedor:{ type: GraphQLString },
        id_operario:{ type: GraphQLString },

        
    })
})
module.exports = cotizacion;