const get_cotizacion = async (parent, args, context, info) => {
    const client = await context.db.connect()
    try{
        const res = await client.query(`
            SELECT
            C.id_cot ,
            C.puntaje ,
            C.descuento ,
            C.monto_total ,
            C.fecha_entrega_min ,
            C.fecha_entrega_max ,
            C.fecha_envio_prov ,
            C.fecha_recibo_prov ,
            C.fecha_envio_op ,
            C.fecha_recibo_op ,
            EC.id_estado_cotizacion ,
            EC.descripcion,
            C.id_pedido ,
            C.nro_requerimiento ,
            C.id_proveedor ,
            C.id_operario
            FROM cotizacion C
            INNER JOIN estado_cotizacion EC
            ON EC.id_estado_cotizacion=C.id_estado_cotizacion;
        `);
        
        const cotizaciones = []
        for(const row of res.rows){
            cotizaciones.push({
                id_cot: row.id_orden_compra,
                puntaje: row.puntaje,
                descuento: row.descuento,
                monto_total: row.monto_total,
                fecha_entrega_min: row.fecha_entrega_min,
                fecha_entrega_max: row.fecha_entrega_max,
                fecha_envio_prov: row.fecha_envio_prov,
                fecha_recibo_prov: row.fecha_recibo_prov,
                fecha_envio_op: row.fecha_envio_op,
                fecha_recibo_op: row.fecha_recibo_op,
                estado_cotizacion:{
                    id_estado_cotizacion:row.id_estado_cotizacion,
                    descripcion: row.descripcion
                },
                id_pedido: row.id_pedido,
                nro_requerimiento: row.nro_requerimiento,
                id_proveedor: row.id_proveedor,
                id_operario: row.id_operario
            })
        }

        return cotizaciones
    }catch(e){
        console.log(e)
    }finally {
        client.release()
    }
}

module.exports = {
    get_cotizacion
};