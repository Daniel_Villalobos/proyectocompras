
const { GraphQLObjectType } = require('graphql')
const query_tipo_documento_identificacion = require('./tipo_documento_identificacion/query')
const query_documento_identificacion = require('./documento_identificacion/query')
const query_usuario = require('./usuario/query')
const query_rol_operario = require('./rol_operario/query')
const query_operario_compra = require('./operario_compra/query')
const query_tipo_contacto = require('./tipo_contacto/query')
const query_contacto = require('./contacto/query')
const query_empresa = require('./empresa/query')
const query_area = require('./area/query')
const query_metodo = require('./metodo/query')
const query_operacion = require('./operacion/query')
const query_operacion_metodo = require('./operacion_metodo/query')
const query_transaccion = require('./transaccion/query')
const query_transaccion_operacion = require('./transaccion_operacion/query')
const query_nacionalidad = require('./nacionalidad/query')
const query_proveedor = require('./proveedor/query')
const query_rubro = require('./rubro/query')
const query_familia = require('./familia/query')
const query_categoria_producto = require('./categoria_producto/query')
const query_tipo_producto = require('./tipo_producto/query')

const query_tipo_compra = require('./tipo_compra/query')
const query_protocolo = require('./protocolo/query')
const query_protocolo_transaccion = require('./protocolo_transaccion/query')
const query_periodo_disponible_prov = require('./periodo_disponible_prov/query')
const query_pedido = require('./pedido/query')
const query_unidad_medida = require('./unidad_medida/query')
const query_requerimiento = require('./requerimiento/query')
const query_estado_aprobacion = require('./estado_aprobacion/query')
const query_aprobacion = require('./aprobacion/query')
const query_estado_cotizacion = require('./estado_cotizacion/query')
const query_cotizacion = require('./cotizacion/query')
const query_orden_compra = require('./orden_de_compra/query')
const query_tipo_reclamo = require('./tipo_reclamo/query')
const query_reclamo = require('./reclamo/query')

const query_estado_producto = require('./estado_producto/query')
const query_proveedor_tipo_producto = require('./proveedor_tipo_producto/query')
const query_objeto_gasto = require('./objeto_gasto/query')
const query_partida_presupuestaria =require('./partida_presupuestaria/query')
const query_tipo_naturaleza = require ('./tipo_naturaleza/query')
const query_tipo_cuantia = require ('./tipo_cuantia/query')
const query_tipo_nacionalidad = require('./tipo_nacionalidad/query')
const query_tipo_urgencia = require('./tipo_urgencia/query')




const rootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: () => ({
      ...query_tipo_documento_identificacion,
      ...query_documento_identificacion,
      ...query_usuario,
      ...query_rol_operario,
      ...query_operario_compra,
      ...query_tipo_contacto,
      ...query_contacto,
      ...query_empresa,
      ...query_area,
      ...query_metodo,
      ...query_operacion,
      ...query_operacion_metodo,
      ...query_transaccion,
      ...query_transaccion_operacion,
      ...query_nacionalidad,
      ...query_proveedor,
      ...query_rubro,
      ...query_familia,
      ...query_categoria_producto,
      ...query_tipo_producto,
      ...query_tipo_compra,
      ...query_protocolo,
      ...query_protocolo_transaccion,
      ...query_periodo_disponible_prov,
      ...query_pedido,
      ...query_unidad_medida,
      ...query_requerimiento,
      ...query_estado_aprobacion,
      ...query_aprobacion,
      ...query_estado_cotizacion,
      ...query_cotizacion,
      ...query_orden_compra,
      ...query_tipo_reclamo,
      ...query_reclamo,
      ...query_estado_producto,
      ...query_proveedor_tipo_producto, 
      ...query_objeto_gasto,
      ...query_partida_presupuestaria, 
      ...query_tipo_naturaleza, 
      ...query_tipo_cuantia, 
      ...query_tipo_nacionalidad,
      ...query_tipo_urgencia
    })
  });
module.exports = rootQuery;
