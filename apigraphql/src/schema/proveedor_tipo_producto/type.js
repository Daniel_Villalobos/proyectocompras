const {GraphQLObjectType, GraphQLString, GraphQLInt } = require('graphql')

const proveedor_tipo_producto = new GraphQLObjectType({
    name: 'proveedor_tipo_producto',
    fields: () => ({
        id_proveedor: { type: GraphQLString },
        id_tipo_prod: {type: GraphQLString},
        cantidad: { type: GraphQLInt }
    })
})
module.exports = proveedor_tipo_producto;