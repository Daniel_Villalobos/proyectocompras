const { GraphQLList } = require('graphql')
const ptp_controller = require('./ptp.controllers')
const type_proveedor_tipo_producto = require('./type')

const query = {
    type_proveedor_tipo_producto: {
        type: new GraphQLList(type_proveedor_tipo_producto),
        resolve: ptp_controller.get_proveedor_tipo_producto
    }
}
module.exports = query;