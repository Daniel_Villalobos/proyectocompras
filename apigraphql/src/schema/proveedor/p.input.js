const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLFloat } = require("graphql")

const proveedorInput = new GraphQLInputObjectType({
    name: 'proveedorInput',
    fields: {
        id_proveedor: { type: GraphQLString, require :true  },
        id_nacionalidad: { type: GraphQLString, require :true  },
        cotizaciones_ganadas: { type: GraphQLInt },
        entregas_cumplidas: { type: GraphQLInt },
        entregas_no_cumplidas: { type: GraphQLInt },
        entregas_destiempo: { type: GraphQLInt },
        cant_cotizaciones: { type: GraphQLInt },
        fecha_reg_prov: { type: GraphQLString },
        calif_prov: { type: GraphQLFloat }
    }
})
module.exports = proveedorInput;