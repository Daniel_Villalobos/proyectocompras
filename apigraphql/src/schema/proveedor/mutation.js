const p_controller = require('./p.controllers');
const p_input = require('./p.input');
const { GraphQLString } = require("graphql");
const mutation = {
    create_proveedor: {
        type: GraphQLString,
        args: {
            proveedor: { type: p_input, required: true}
        },
        resolve: p_controller.create_proveedor
    }
}
module.exports = mutation;